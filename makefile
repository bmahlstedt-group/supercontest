##############################################################################
# Variable initialization.
##############################################################################

SRC=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PYPROJECT_TOML=$(SRC)/pyproject.toml
APP=$(SRC)/supercontest
APP_CONTAINER=supercontest-app
COMPOSE_FILE_DEV=$(SRC)/docker/docker-compose-dev.yml
COMPOSE_FILE_PROD=$(SRC)/docker/docker-compose-prod.yml
DB_ENV_PRIVATE=$(SRC)/supercontest/config/db/private.conf
DB_PGPASS_PROD=$(SRC)/supercontest/config/db/private.pgpass
DB_ENV_PUBLIC_DEV=$(SRC)/supercontest/config/db/public-dev.conf
DB_ENV_PUBLIC_PROD=$(SRC)/supercontest/config/db/public-prod.conf
DB_CONTAINER_DEV=supercontest-database
DB_HOST_PROD=supercontest.cm7siuqequ29.us-west-1.rds.amazonaws.com
DB_USER=supercontest
DB_NAME=supercontest
DB_DUMP=$(SRC)/backups/supercontest.dump
CACHE_ENV_PRIVATE=$(SRC)/supercontest/config/cache/private.conf
CACHE_HOST_PROD=clustercfg.supercontest.buigtg.usw1.cache.amazonaws.com
CACHE_PORT_PROD=6582
DOCSOURCE=$(SRC)/docs
DOCBUILD=$(SRC)/_readthedocs/html
JUNITXML=$(SRC)/junit.xml

##############################################################################
# Managing state of containers. Prerequisite to most.
# This is the only section that uses docker compose, since app/db containers
# depend on one another. As such, targets are separate for dev and prod.
##############################################################################

# Up the containers.
.PHONY: start-dev
start-dev:
	docker compose -f $(COMPOSE_FILE_DEV) up -d
.PHONY: start-prod
start-prod:
	docker compose -f $(COMPOSE_FILE_PROD) up -d

# Up the db container only (only avail in dev)
.PHONY: start-db-dev
start-db-dev:
	docker compose -f $(COMPOSE_FILE_DEV) up -d $(DB_CONTAINER_DEV)

# Build and then start the containers.
.PHONY: build-start-dev
build-start-dev:
	docker compose -f $(COMPOSE_FILE_DEV) up -d --build
.PHONY: build-start-prod
build-start-prod:
	docker compose -f $(COMPOSE_FILE_PROD) up -d --build

# Restart the app container.
.PHONY: restart-dev
restart-dev:
	docker compose -f $(COMPOSE_FILE_DEV) restart $(APP_CONTAINER)
.PHONY: restart-prod
restart-prod:
	docker compose -f $(COMPOSE_FILE_PROD) restart $(APP_CONTAINER)

# Stop the containers.
.PHONY: stop-dev
stop-dev:
	docker compose -f $(COMPOSE_FILE_DEV) stop
.PHONY: stop-prod
stop-prod:
	docker compose -f $(COMPOSE_FILE_PROD) stop

# Stop and remove the containers (and volumes/networks/etc).
.PHONY: down-dev
down-dev:
	docker compose -f $(COMPOSE_FILE_DEV) down
.PHONY: down-prod
down-prod:
	docker compose -f $(COMPOSE_FILE_PROD) down

##############################################################################
# Useful commands to interact with the app.
##############################################################################

# Show logs for the app container.
.PHONY: logs
logs:
	docker logs -f $(APP_CONTAINER)

# Enters a python shell, in the app container, in the PYTHON env.
.PHONY: python
python:
	docker exec -it $(APP_CONTAINER) poetry run python

# Enters a bash shell, in the app container, with the PYTHON env activated.
.PHONY: bash
bash:
	docker exec -it $(APP_CONTAINER) /bin/sh -c "poetry shell"

# Enters a python shell, in the app container, in the PYTHON env,
# with all of the flask app loaded (has current_app, etc).
.PHONY: flask-python
flask-python:
	docker exec -it $(APP_CONTAINER) /bin/sh -c "FLASK_APP=supercontest FLASK_CLI=1 poetry run flask shell"

# Enters a bash shell, in the app container, with the PYTHON env activated,
# with the flask CLI available (to run `flask db <>` commands, etc).
.PHONY: flask-bash
flask-bash:
	docker exec -it $(APP_CONTAINER) /bin/sh -c "FLASK_APP=supercontest FLASK_CLI=1 poetry shell"

##############################################################################
# Useful commands to interact with the database. Queries, backups, restores.
# You shouldn't really need the backup/restore functionality anymore. Dev
# doesn't require it, and prod is handled by RDS. They're mostly exemplary.
##############################################################################

# Enters a psql context so you can run direct queries.
# Dev is local to the db container.
.PHONY: psql-dev
psql-dev:
	docker exec -it --env-file $(DB_ENV_PUBLIC_DEV) --env-file $(DB_ENV_PRIVATE) $(DB_CONTAINER_DEV) psql -U $(DB_USER) -d $(DB_NAME)

# Enters a psql context so you can run direct queries.
# Prod hits the RDS instance. Must be run from EC2.
.PHONY: psql-prod
psql-prod:
	PGPASSFILE=${DB_PGPASS_PROD} psql -h ${DB_HOST_PROD} -U $(DB_USER) -d $(DB_NAME)

# Creates a local backup of the local db. Generically named (so will overwrite).
.PHONY: backup-db-dev
backup-db-dev:
	docker exec --env-file $(DB_ENV_PUBLIC_DEV) --env-file $(DB_ENV_PRIVATE) $(DB_CONTAINER_DEV) pg_dump --clean --format=c -U $(DB_USER) -d $(DB_NAME) > $(DB_DUMP)

# Run to create a local backup of the remote db. Generically named (so will overwrite).
.PHONY: backup-db-prod
backup-db-prod:
	PGPASSFILE=${DB_PGPASS_PROD} pg_dump --clean --format=c -h ${DB_HOST_PROD} -U $(DB_USER) -d $(DB_NAME) > $(DB_DUMP)

# Restore the local db from the local backup.
# pg_restore is intentionally forceful, dropping before recreating. Because of
# the DROP command, no connections to the supercontest db can be open. This
# means we have to kill the flask container, which is connected to the db,
# then restart. The next targets are the wrappers which handle that.
# The "-i" is required for this exec because docker needs to know to read stdin.
.PHONY: restore-db-dev
restore-db-dev:
	make stop-dev
	make start-db-dev
	echo waiting for postgres 5432 to be ready
	sleep 2
	docker exec -i --env-file $(DB_ENV_PUBLIC_DEV) --env-file $(DB_ENV_PRIVATE) $(DB_CONTAINER_DEV) pg_restore --clean --create -U $(DB_USER) -d postgres < $(DB_DUMP)
	make start-dev

# Restore the remote db from the local backup.
# Prod hits the RDS instance. Must be run from EC2.
# Same applies here. Kill all app connections before running, or pg won't allow drop db.
.PHONY: restore-db-prod
restore-db-prod:
	make stop-prod
	PGPASSFILE=${DB_PGPASS_PROD} pg_restore --clean --create -h ${DB_HOST_PROD} -U $(DB_USER) -d postgres $(DB_DUMP)
	make start-prod

# SSHs into the prod EC2, runs a pg_dump against the RDS DB, then copies
# the dump back here and restores the local db.
.PHONY: sync-db-dev
sync-db-dev:
	ssh sbsc 'cd code/supercontest && make backup-db-prod'
	scp sbsc:code/supercontest/backups/supercontest.dump backups/supercontest.dump
	make restore-db-dev

##############################################################################
# Useful commands to interact with the cache. This is for redis-cli.
# For the python interface, use flask-python (and then the cache object).
##############################################################################

# Enters an authenticated redis-cli shell. This must be run from the EC2 instance.
# The auth (pw) is passed from the private conf file included at the top of this makefile.
.PHONY: redis-cli-prod
redis-cli-prod:
	. $(CACHE_ENV_PRIVATE) && redis-cli -c -h $(CACHE_HOST_PROD) --tls -p $(CACHE_PORT_PROD) -a $$REDISCLI_AUTH

##############################################################################
# Testing.
##############################################################################

.PHONY: testenv
testenv:
	poetry install --with dev,test --sync

.PHONY: style
style: testenv
	poetry run black $(APP)

.PHONY: lint
lint: testenv
	poetry run pylint $(APP)

.PHONY: type
type: testenv
	poetry run pyright $(APP)

.PHONY: security
security: testenv
	poetry run bandit -c $(PYPROJECT_TOML) -r $(APP)

.PHONY: test
test: testenv
	poetry run pytest --junitxml=$(JUNITXML)

.PHONY: alltests
alltests:
	make style
	make lint
	make type
	make security
	make test

##############################################################################
# Docs.
##############################################################################

.PHONY: docenv
docenv:
	poetry install --with docs --sync

.PHONY: docs
docs: docenv
	poetry run sphinx-build -a -j auto $(DOCSOURCE) $(DOCBUILD)
