"""Tests for memory leaks in the flask app.

1) Go to supercontest/views/main.py and put this block at the top of the file:

import tracemalloc
import os
import psutil
process = psutil.Process(os.getpid())
tracemalloc.start()
snapshot = None

2) Go to supercontest/views/main.py and put this block at the bottom of the file:

@main_blueprint.route('/memory-print')
def memory_print():
    return {'memory': process.memory_info().rss}

@main_blueprint.route('/memory-snapshot')
def memory_snapshot():
    global snapshot
    if not snapshot:
        snapshot = tracemalloc.take_snapshot()
        return 'took snapshot\n'
    else:
        diffs = tracemalloc.take_snapshot().compare_to(snapshot, "lineno")
        top_diffs = [str(diff) for diff in diffs[:5]]
        return "\n".join(top_diffs)

3) make start-dev (start the service)
4) python tests/python/test_mem_leak.py (run this file)

Then fix any discovered mem leaks.
"""
import requests

ROUTES_TO_TEST = [
    'season2022/league0/week6/matchups',
    'season2020/league0/week3/picks',
    'season2021/league0/leaderboard',
]
NUM_CALLS = 50

for route in ROUTES_TO_TEST:
    print('----')
    print(f'Testing route /{route} over {NUM_CALLS} calls')
    for _ in range(10):  # start with some requests to stabilize/initialize
        requests.get(f'http://127.0.0.1:5000/{route}')
    mem_before = requests.get('http://127.0.0.1:5000/memory-print').json().get('memory')
    print(f'Memory before: {int(mem_before)}')
    _ = requests.get('http://127.0.0.1:5000/memory-snapshot')  # take first snapshot to compare against
    for _ in range(NUM_CALLS):
        requests.get(f'http://127.0.0.1:5000/{route}')
    mem_after = requests.get('http://127.0.0.1:5000/memory-print').json().get('memory')
    print(f'Memory after: {int(mem_after)}')
    resp = requests.get('http://127.0.0.1:5000/memory-snapshot')  # compares within the function
    print('Top 5 increases in mem by line:')
    print(resp.text)
