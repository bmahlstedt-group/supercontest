"""The primary tests of the frontend views. This starts an app. Remember it is still just a
unittest suite though; it does not run integration tests with a database.
"""
import pytest
from flask import Flask
from flask.testing import FlaskClient
from supercontest import create_app


@pytest.fixture()
def app() -> Flask:
    """The app factory. Can change to a generator if you have extra teardown steps added later
    which follow the ``yield app``.

    :return: Flask app.
    """
    app = create_app(test=True)
    return app


@pytest.fixture()
def client(app: Flask) -> FlaskClient:
    """Helper fixture for the werkzeug client.

    :param app: Flask app.
    :return: Flask client.
    """
    return app.test_client()


def test_routes(app: Flask, client: FlaskClient):
    endpoints = [rule.endpoint for rule in app.url_map.iter_rules()]
    assert "leaderboard.leaderboard" in endpoints
    assert client.application.name == "supercontest"
