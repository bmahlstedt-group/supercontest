"""Email-related utilities. These functions all respect user notification preferences."""

import logging
from typing import Any
from os.path import basename
from flask import current_app, url_for, request
from flask_mail import Mail, Message
from supercontest.dbsession import queries

logger = logging.getLogger(__name__)

#: The mail object used throughout the app to send email.
mail = Mail()


def get_footer() -> str:
    """Defined with a function instead of a constant so it's not invoked at import,
    where ``url_for`` errors without an app context.

    :return: Common email footer for the app.
    """
    return (
        "\n\n"
        "\n---\n"
        f"this is an automated message - visit {url_for('main.email_preferences', _external=True)} "
        "to unsubscribe or change your notification settings"
        "\n---\n"
    )


def send_mail(subject: str, body: str, recipients: list[str], png_attachment: str | None = None):
    """Send email to any number of users, via bcc.

    :param subject: The subject of the email.
    :param body: The body of the email.
    :param recipients: Email addresses of all desired recipients.
    :param png_attachment: Path to a ``.png`` file to attach, if desired.
    """
    try:
        cfg: Any = current_app.config
        if cfg.get("DEBUG", True):
            logger.debug("Not emailing when the app is in dev mode")
            return
    except:
        pass
    msg = Message(subject, bcc=recipients)
    msg.body = body
    if png_attachment:
        with open(png_attachment, "rb") as handle:
            msg.attach(basename(png_attachment), "image/png", handle.read())
    mail.send(msg)


def email_picks(
    season: int,
    week: int,
    teams: list[str],
    emails: list[str],
    email_admin: bool = True,
):
    """Emails a user a list of teams. This is usually to notify the single user that their late
    picks have been submitted.

    :param season: Goes in the subject of the email.
    :param week: Goes in the subject of the email.
    :param teams: The names of the teams to send.
    :param emails: The regular recipients of the email.
    :param email_admin: If ``True``, sends an identical email to notify the admin.
    """
    subject = f"Supercontest {season} Week {week} - Picks"
    body_people = "Submitted the following picks for: " + ", ".join(emails)
    body_teams = "\n".join(teams)
    body = body_people + "\n\n" + body_teams
    send_mail(subject=subject, body=body, recipients=emails)
    if email_admin:
        send_mail(subject=subject, body=body, recipients=queries.get_admin_emails())


def email_picks_open(season: int | None, week: int | None):
    """Emails all subscribed users that new lines have been posted.

    :param season: Goes in the subject of the email.
    :param week: Goes in the subject of the email.
    """
    subject = f"Supercontest {season} Week {week} - Picks Open"
    try:
        link = request.url_root
        footer = get_footer()
    except:
        link = "the site"
        footer = ""
    lines = [
        "The new lines for this week have been posted.",
        f"Log in to {link} and make your picks!",
    ]
    body = "\n\n".join(lines) + footer
    users = queries.get_users_with_email_pref(pref="email_when_picks_open")
    send_mail(subject=subject, body=body, recipients=users)


def email_picks_closing(season: int | None, week: int | None, unfinished_pickers: list[str]):
    """Emails all subscribed users that lockdown is about to happen.

    :param season: Goes in the subject of the email.
    :param week: Goes in the subject of the email.
    :param unfinished_pickers: All users who have not submitted 5 picks. This function does not
        calculate it, of course - it's just passed.
    """
    # First, email the late pickers.
    subject = f"Supercontest {season} Week {week} - Picks Closing"
    lines = [
        "You have not submitted 5 picks for this week yet, and lockdown is at midnight tonight.",
        f"Log in to {request.url_root} and select your teams!",
    ]
    body = "\n\n".join(lines) + get_footer()
    subscribed_users = queries.get_users_with_email_pref(pref="email_when_picks_closing")
    users = list(set(unfinished_pickers).intersection(subscribed_users))
    send_mail(subject=subject, body=body, recipients=users)
    # Second, email the admin with a list of the late pickers.
    subject = f"Supercontest {season} Week {week} - Late Pickers"
    lines = [
        "The app just sent a reminder email to the following people:",
        "\n".join(users),
    ]
    body = "\n\n".join(lines)
    admin = queries.get_admin_emails()
    send_mail(subject=subject, body=body, recipients=admin)


def email_all_picks(season: int, week: int, png: str):
    """Emails an image of the table of all the league picks for the week.

    :param season: Goes in the subject of the email.
    :param week: Goes in the subject of the email.
    :param png: Path to the png to be attached.
    """
    subject = f"Supercontest {season} Week {week} - All Picks"
    lines = [
        "Picks are locked!",
        "The table of all user picks for this week is located at "
        + f"{url_for('picks.picks', season=season, league=0, week=week)}",
        "It is attached below as well.",
    ]
    body = "\n\n".join(lines) + get_footer()
    users = queries.get_users_with_email_pref(pref="email_all_picks")
    send_mail(subject=subject, body=body, recipients=users, png_attachment=png)
