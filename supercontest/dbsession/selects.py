"""Actual reads to the cache/db.

Example for raw SQL via ORM:
``season_id = db.session.scalar(text("SELECT last_value FROM seasons_id_seq"))``
"""
from datetime import datetime
from sqlalchemy import select
from supercontest.models import (
    db,
    Role,
    League,
    User,
    Season,
    Week,
    Team,
    Prediction,
    Location,
    Status,
    Game,
    Contestant,
    Pick,
)


def get_role_from_name(name: str) -> Role:
    """Returns the id for a Role row, given its name.

    :param name: Name of the Role to lookup.
    :return: The Role object.
    """
    stmt = select(Role).where(Role.name == name)
    return db.session.scalars(stmt).one()


def get_user_from_email(email: str) -> User:
    """Takes an email, returns a user object. This is case-insensitive. If the email address has
    been lowered, like the ``admin.late_picks`` form does, it will still find the right user.

    :param email: Email.
    :return: The user object.
    """
    stmt = select(User).where(User.email.ilike(email))
    return db.session.scalars(stmt).one()


def get_all_users() -> list[User]:
    """Simply gives the full list of user objects.

    :return: All user objects in the database.
    """
    stmt = select(User)
    return db.session.scalars(stmt).all()


def get_users_in_league(league_id: int) -> list[User]:
    """Returns a list of users in a given league.

    :param league_id: The league ID.
    :return: All users in the league.
    """
    stmt = select(User).where(User.leagues.any(id=league_id))
    return db.session.scalars(stmt).all()


def get_admin() -> list[User]:
    """Gives list of users with the 'Admin' role.

    :return: All admin user objects.
    """
    stmt = select(User).where(User.roles.any(name="Admin"))
    return db.session.scalars(stmt).all()


def get_week(season: int | None, week: int | None) -> Week:
    """Returns the week obj, given its number and the season year.

    :param season: Season year.
    :param week: Week number.
    :return: Week obj.
    """
    stmt = select(Week).join(Season).where(Season.season == season, Week.week == week)
    return db.session.scalars(stmt).one()


def get_sorted_seasons() -> list[Season]:
    """Numerically sorted season objects, ascending.

    :return: All seasons, from oldest to newest.
    """
    stmt = select(Season).order_by(Season.season)
    return db.session.scalars(stmt).all()


def get_sorted_weeks(season: int) -> list[Week]:
    """Numerically sorted week objects for a season, ascending.

    :param season: Season year.
    :return: All weeks, from oldest to newest.
    """
    stmt = select(Week).join(Season).where(Season.season == season).order_by(Week.week)
    return db.session.scalars(stmt).all()


def get_season_by_time(dt_obj: datetime) -> Season | None:
    """Returns the season object for a given datetime. This is useful for determining the current
    season.

    :param dt_obj: The datetime to check.
    :return: The season object.
    """
    stmt = select(Season).where(Season.season_start <= dt_obj, Season.season_end >= dt_obj)
    return db.session.scalars(stmt).one_or_none()


def get_week_by_time(dt_obj: datetime) -> Week | None:
    """Returns the week object for a given datetime. This is useful for determining the current
    week.

    :param dt_obj: The datetime to check.
    :return: The week object.
    """
    stmt = select(Week).where(Week.week_start <= dt_obj, Week.week_end >= dt_obj)
    return db.session.scalars(stmt).one_or_none()


def get_leagues(season: int | None) -> list[League]:
    """Get leagues associated with a season.

    :param season: Season year.
    :return: League objects.
    """
    stmt = select(League).join(Season).where(Season.season == season)
    return db.session.scalars(stmt).all()


def get_paid_league_for_season(season: int | None) -> League:
    """Returns the official paid league for a specific season. This is denoted by the league name
    being "Paid" - if there isn't one with that name for that season, this errors. If multiple
    exist (should never happen), this takes the first one. Raises an error (via ``one()``) if
    there are no paid leagues for that season.

    :param season: Season year.
    :return: The paid league.
    """
    stmt = select(League).join(Season).where(League.name == "Paid", Season.season == season)
    return db.session.scalars(stmt).one()


def get_team(name: str) -> Team:
    """Returns the team object, given its name. Compares with ``ilike``, so the argument can be
    capitalized, lowered, uppered, etc.

    :param name: The name of the team.
    :return: Team object.
    """
    stmt = select(Team).where(Team.name.ilike(name))
    return db.session.scalars(stmt).one()


def get_teams() -> list[Team]:
    """Gets a list of all team objects available to the app.

    :return: All objects from the teams table.
    """
    stmt = select(Team)
    return db.session.scalars(stmt).all()


def get_predictions() -> list[Prediction]:
    """Gets a list of all prediction objects available to the app.

    :return: All objects from the predictions table.
    """
    stmt = select(Prediction)
    return db.session.scalars(stmt).all()


def get_locations() -> list[Location]:
    """Gets a list of all location objects available to the app.

    :return: All objects from the locations table.
    """
    stmt = select(Location)
    return db.session.scalars(stmt).all()


def get_statuses() -> list[Status]:
    """Gets a list of all game status objects available to the app.

    :return: All objects in the statuses table.
    """
    stmt = select(Status)
    return db.session.scalars(stmt).all()


def get_games() -> list[Game]:
    """Gets a list of all game objects available to the app.

    :return: All objects in the games table.
    """
    stmt = select(Game)
    return db.session.scalars(stmt).all()


def get_games_in_season(season: int) -> list[Game]:
    """Gets a list of all game objects in a given season.

    :param season: Season year.
    :return: All objects in the games table.
    """
    stmt = select(Game).join(Week).join(Season).where(Season.season == season)
    return db.session.scalars(stmt).all()


def get_games_in_week(season: int, week: int) -> list[Game]:
    """Gets a list of all game objects in a given week.

    :param season: Season year.
    :param week: Week number.
    :return: All objects in the games table.
    """
    stmt = select(Game).join(Week).join(Season).where(Season.season == season, Week.week == week)
    return db.session.scalars(stmt).all()


def get_contestants() -> list[Contestant]:
    """Gets a list of all contestant objects available to the app.

    :return: All objects in the contestants table.
    """
    stmt = select(Contestant)
    return db.session.scalars(stmt).all()


def get_contestants_in_week(season: int, week: int) -> list[Contestant]:
    """Gets a list of all contestant objects in a given week.

    :param season: Season year.
    :param week: Week number.
    :return: All objects in the contestants table.
    """
    stmt = (
        select(Contestant)
        .join(Game)
        .join(Week)
        .join(Season)
        .where(Season.season == season, Week.week == week)
    )
    return db.session.scalars(stmt).all()


def get_picks() -> list[Pick]:
    """Gets a list of all pick objects available to the app.

    :return: All objects in the picks table.
    """
    stmt = select(Pick)
    return db.session.scalars(stmt).all()


def get_picks_in_season(season: int) -> list[Pick]:
    """Gets a list of all pick objects in a given season.

    :param season: Season year.
    :return: All objects in the picks table.
    """
    stmt = (
        select(Pick)
        .join(Contestant)
        .join(Game)
        .join(Week)
        .join(Season)
        .where(Season.season == season)
    )
    return db.session.scalars(stmt).all()


def get_picks_in_week(season: int, week: int) -> list[Pick]:
    """Gets a list of all pick objects in a given week.

    :param season: Season year.
    :param week: Week number.
    :return: All objects in the picks table.
    """
    stmt = (
        select(Pick)
        .join(Contestant)
        .join(Game)
        .join(Week)
        .join(Season)
        .where(Season.season == season, Week.week == week)
    )
    return db.session.scalars(stmt).all()
