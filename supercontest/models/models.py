# pylint: disable=too-few-public-methods,missing-class-docstring,missing-param-doc
"""The data schema for the application.

There are one-to-one, one-to-many, many-to-one, and many-to-many relationships below.
*Columns* have relationships, not tables, of course. For tables that are "simply" associated with
others, you can make the statements above in a "simple" sense - eg ``Season`` to ``Week`` is a
one-to-many relationship. But more specificity is required for tables the have relationships
across multiple columns - eg games and (2) contestants.

In the SQL layer, all FKs manifest as actual columns and all association tables manifest as actual
tables (of course). All integer PKs have associated sequences and column defaults (to nextval)
in postgres.

In the ORM layer, the relationships are all queryable as if they were real columns (of course).
I have made every relationship bidirectional.

Observers are used for computed cols. In some cases, it's a basic calculation (eg DOW from
datetime). In others, it's an FK and a relationship (eg coverer from games). All are stored, not
virtual. They're calculated when the observed cols (across tables) change, on write (not read).
**Important**: Observers double as defaults and thus will run at insert time. So if a relation is
observed, it needs to be passed at instantiation (rather than letting the ORM associate it with
the FK after creation).

In general, this module flows from children -> parents, to make dependency readable + ordered.
"""
from typing import Any
from datetime import date, datetime as dt
from flask_user import UserMixin
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey, Table, Column, Integer, DateTime
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy_utils import observes
from supercontest.util.timing import get_dow
from supercontest.util.math import bucket_number


#: The db object used throughout the app to interact with the session; query, commit, etc.
# The member ``session`` is a scoped session per request (requires a flask app context).
# The member ``Model`` becomes a class populated dynamically at runtime.
# So we annotate ``Any`` in order for the static typechecker to not flag these.
# Note - this hides a LOT for the returns of queries. I wish it weren't the case and we could
# statically typecheck all the returns of select/execute based on the types of the mapped cols
# in the models. Oh well. Models and their cols are still properly typechecked, of course.
db: Any = SQLAlchemy()

#: The cache object used to memoize query functions and cache view functions.
cache: Any = Cache()


class Season(db.Model):
    __tablename__ = "seasons"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    season: Mapped[int]
    season_start: Mapped[date]
    season_end: Mapped[date]
    # Backward relationships.
    weeks: Mapped[list["Week"]] = relationship(back_populates="season")
    leagues: Mapped[list["League"]] = relationship(back_populates="season")


class Week(db.Model):
    __tablename__ = "weeks"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    week: Mapped[int]
    week_start: Mapped[dt] = mapped_column(DateTime(timezone=True))
    week_end: Mapped[dt] = mapped_column(DateTime(timezone=True))
    # FK cols.
    season_id: Mapped[int] = mapped_column(ForeignKey("seasons.id"))
    # Forward relationships.
    season: Mapped["Season"] = relationship(back_populates="weeks")
    # Backward relationships.
    games: Mapped[list["Game"]] = relationship(back_populates="week")


#: The many-many relationship and its association table between User and Role.
role_user = Table(
    "role_user_association",
    db.Model.metadata,
    Column("role_id", Integer, ForeignKey("roles.id"), primary_key=True),
    Column("user_id", Integer, ForeignKey("users.id"), primary_key=True),
)

#: The many-many relationship and its association table between User and League.
league_user = Table(
    "league_user_association",
    db.Model.metadata,
    Column("league_id", Integer, ForeignKey("leagues.id"), primary_key=True),
    Column("user_id", Integer, ForeignKey("users.id"), primary_key=True),
)


class Role(db.Model):
    __tablename__ = "roles"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    # Backward relationships.
    users: Mapped[list["User"]] = relationship(back_populates="roles", secondary=role_user)


class League(db.Model):
    __tablename__ = "leagues"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    # FK cols.
    season_id: Mapped[int] = mapped_column(ForeignKey("seasons.id"))
    # Forward relationships.
    season: Mapped["Season"] = relationship(back_populates="leagues")
    # Backward relationships.
    users: Mapped[list["User"]] = relationship(back_populates="leagues", secondary=league_user)


class User(db.Model, UserMixin):
    __tablename__ = "users"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    active: Mapped[bool] = mapped_column("is_active", default=True)
    email: Mapped[str] = mapped_column(unique=True)
    email_confirmed_at: Mapped[dt | None] = mapped_column(DateTime(timezone=True))
    password: Mapped[str]
    first_name: Mapped[str | None]
    last_name: Mapped[str | None]
    email_when_picks_open: Mapped[bool] = mapped_column(default=True)
    email_when_picks_closing: Mapped[bool] = mapped_column(default=True)
    email_all_picks: Mapped[bool] = mapped_column(default=True)
    # Forward relationships. The FKs are kept in the association table for this many-to-many.
    roles: Mapped[list["Role"]] = relationship(back_populates="users", secondary=role_user)
    leagues: Mapped[list["League"]] = relationship(back_populates="users", secondary=league_user)
    # Backward relationships.
    picks: Mapped[list["Pick"]] = relationship(back_populates="user")


class Team(db.Model):
    __tablename__ = "teams"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    abbv: Mapped[str]
    city: Mapped[str]
    espn_city: Mapped[str]
    # Backward relationships.
    contestants: Mapped[list["Contestant"]] = relationship(back_populates="team")


class Prediction(db.Model):
    __tablename__ = "predictions"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    # Backward relationships.
    contestants: Mapped[list["Contestant"]] = relationship(back_populates="prediction")


class Location(db.Model):
    __tablename__ = "locations"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    # Backward relationships.
    contestants: Mapped[list["Contestant"]] = relationship(back_populates="location")


class Coverage(db.Model):
    __tablename__ = "coverages"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    points: Mapped[float]
    # Backward relationships.
    contestants: Mapped[list["Contestant"]] = relationship(back_populates="coverage")


class Status(db.Model):
    __tablename__ = "statuses"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    abbv: Mapped[str]
    espn_abbv: Mapped[str]
    unstarted: Mapped[bool]
    inprogress: Mapped[bool]
    finished: Mapped[bool]
    # Backward relationships.
    games: Mapped[list["Game"]] = relationship(back_populates="status")


class Game(db.Model):
    __tablename__ = "games"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    datetime: Mapped[dt] = mapped_column(DateTime(timezone=True))
    line: Mapped[float]
    # FK cols.
    week_id: Mapped[int] = mapped_column(ForeignKey("weeks.id"))
    status_id: Mapped[int] = mapped_column(ForeignKey("statuses.id"))
    # Forward relationships.
    week: Mapped["Week"] = relationship(back_populates="games")
    status: Mapped["Status"] = relationship(back_populates="games")
    # Backward relationships.
    contestants: Mapped[list["Contestant"]] = relationship(back_populates="game")

    def get_contestants(
        self,
        by: str,  # pylint: disable=invalid-name
        fail: bool = True,
    ) -> tuple[Any, ...]:
        """Returns the Contestant objects in the matchup, by various classifications.

        :param by: "prediction" or "location" or "coverage"
        :param fail: If True, will error if pickem/neutral/push. Else, will return in order of
            contestants[0], contestants[1]
        :return: (favorite, underdog) or (home, visitor) or (coverer, noncoverer)
        :raises ValueError: If ``fail=True`` and no favorite/home/coverer in this game.
        """
        if len(self.contestants) != 2:
            raise ValueError("This game does not have exactly 2 opponents.")
        options = {  # in order of positive, negative
            "prediction": ("Favorite", "Underdog"),
            "location": ("Home", "Visitor"),
            "coverage": ("Cover", "Noncover"),
        }
        # Fail fast if this matchup has a neutral value for this attribute.
        if fail is True and getattr(self.contestants[0], by).name not in options[by]:
            raise ValueError("This game has no (favored/home/covering) team.")
        # Default this order if contestants[0] is the positive, or fail=False.
        contestants = self.contestants[0], self.contestants[1]
        # If contestants[0] is the negative, switch order.
        if getattr(self.contestants[0], by).name == options[by][1]:
            contestants = contestants[::-1]
        return contestants

    def get_dow(self) -> str:
        """Takes the datetime col of this instance and simply returns the day of the week.

        :return: DOW.
        """
        return get_dow(self.datetime)


class Contestant(db.Model):
    __tablename__ = "contestants"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    score: Mapped[int]
    opponent_score: Mapped[int]  # denormalized intentionally to calc margin/coverage
    margin: Mapped[float]  # set by observer
    # FK cols.
    game_id: Mapped[int] = mapped_column(ForeignKey("games.id"))
    team_id: Mapped[int] = mapped_column(ForeignKey("teams.id"))
    prediction_id: Mapped[int] = mapped_column(ForeignKey("predictions.id"))
    location_id: Mapped[int] = mapped_column(ForeignKey("locations.id"))
    coverage_id: Mapped[int] = mapped_column(ForeignKey("coverages.id"))  # set by observer
    # Forward relationships.
    game: Mapped["Game"] = relationship(back_populates="contestants")
    team: Mapped["Team"] = relationship(back_populates="contestants")
    prediction: Mapped["Prediction"] = relationship(back_populates="contestants")
    location: Mapped["Location"] = relationship(back_populates="contestants")
    coverage: Mapped["Coverage"] = relationship(back_populates="contestants")
    # Backward relationships.
    picks: Mapped[list["Pick"]] = relationship(back_populates="contestant")

    @observes("score", "opponent_score", "game.line", "prediction_id")
    def set_coverage(self, score: int, opponent_score: int, line: float, prediction: int) -> None:
        """Margin is the difference in score and margin above line, from the perspective of this
        object. Prediction/Coverage IDs are hardcoded for perf (no query).
        """
        self.margin = score - opponent_score - (line if prediction != 1 else -line)  # negate if dog
        self.coverage_id = 3 if self.margin > 0 else 2 if self.margin == 0 else 1

    def get_opponent(self) -> Any:
        """Don't need it as an ID FK col, or a relationship, because then there are complications
        when creating this object (its opponent ID doesn't exist yet and self-relationships can get
        messy) - so this convenience function just provides it when called.

        :return: The Contestant object for the opponent.
        :raises ValueError: If there's not exactly 2 opponents in the game associated
            with this contestant.
        """
        if len(self.game.contestants) != 2:
            raise ValueError("This game does not have exactly 2 opponents.")
        return (
            self.game.contestants[1]
            if self.id == self.game.contestants[0].id
            else self.game.contestants[0]
        )

    def get_line(self) -> float:
        """Handles the sign of the line from the parent's game obj, whether or not you're the
        favorite or the underdog.

        :return: Signed line. Negative for favorite, positive for underdog, 0 for pickem.
        """
        return self.game.line if self.prediction.name == "Underdog" else -self.game.line

    def get_line_bucket(self) -> str:
        """Groups lines into ranges for analysis with more appropriate precision.

        :return: String range where the value falls.
        """
        return bucket_number(num=self.get_line(), step=3, stop=15)


class Pick(db.Model):
    __tablename__ = "picks"
    # Normal cols.
    id: Mapped[int] = mapped_column(primary_key=True)
    # FK cols.
    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
    contestant_id: Mapped[int] = mapped_column(ForeignKey("contestants.id"))
    # Forward relationships.
    user: Mapped["User"] = relationship(back_populates="picks")
    contestant: Mapped["Contestant"] = relationship(back_populates="picks")
