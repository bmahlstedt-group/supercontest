"""Logic for fetching NFL scores and committing them to the database."""
import logging
import re
from urllib.parse import unquote
import requests
from supercontest.dbsession import commits, queries
from supercontest.models import Game
from supercontest.util.types import ScoresFetch
from supercontest.util.timing import is_around_kickoff

logger = logging.getLogger(__name__)


def fetch_scores(  # pylint: disable=too-many-locals
    sbsc_games: list[Game],
) -> tuple[list[Game], list[ScoresFetch]]:
    """Hits the ESPN API for live score updates and the time status of the game (quarter, final,
    etc). Doesn't need a full webdriver, it's a static xml return.

    Note that ESPN lists by location, which does not differentiate the Los Angeles teams. So we have
    to hit our own DB for the Game objects for this week, to infer the ESPN scores by opponent.

    :param sbsc_games: The response from the game fetch.
    :return: The SBSC Game objects and ESPN score objects, in the same order.
    """
    url = "https://www.espn.com/nfl/bottomline/scores"
    # As of 2023, the espn api 403s without a user agent. This was copied directly from my browser.
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
        + "AppleWebKit/537.36 (KHTML, like Gecko) "
        + "Chrome/116.0.0.0 Safari/537.36",
    }
    resp = requests.get(url, timeout=10, headers=headers)
    games = [unquote(game) for game in re.findall(r"s_left\d*=(.+?)&nfl_s_right", resp.text)]
    espn_city_team_map = queries.get_map_espn_city_team()
    ordered_sbsc_games: list[Game] = []
    ordered_scores: list[ScoresFetch] = []
    for game in games:
        # Extract (CITY+SCORE) information, based on whether game has started or not.
        if " at " in game:  # game hasn't started yet
            # line looks like "Tennessee at Denver (10:10 PM ET)"
            visiting_city, home_str = game.split(" at ")
            home_city = home_str.split(" (")[0]
            home_team_score = 0
            visiting_team_score = 0
            status = "P"
        else:  # game has already begun
            # line looks like "Cleveland 6   Baltimore 17 (00:00 IN 2ND)"
            # or "Houston 20   ^Kansas City 34 (FINAL)"
            teams_scores, status = game.split("(")
            teams_scores = re.split(r"(\d.)", teams_scores)
            visiting_city = teams_scores[0].strip().replace("^", "")
            visiting_team_score = int(teams_scores[1])
            home_city = teams_scores[2].strip().replace("^", "")
            home_team_score = int(teams_scores[3])
            status = status.split()[-1].replace(")", "")
        # Now we need to map (CITY+SCORE) to (TEAM+SCORE), dealing with ESPN's ambiguity of LA.
        # This is also the step in which we map to our own Game obj in the SBSC database.
        if all("Los Angeles" == city for city in [home_city, visiting_city]):
            sbsc_game = [
                _game
                for _game in sbsc_games
                if "Los Angeles"
                in [_game.contestants[0].team.espn_city, _game.contestants[1].team.espn_city]
            ][0]
            home_team_cont_obj, visiting_team_cont_obj = sbsc_game.get_contestants(by="location")
            home_team = home_team_cont_obj.team.name
            visiting_team = visiting_team_cont_obj.team.name
        elif any("Los Angeles" == city for city in [home_city, visiting_city]):
            non_la_city = [city for city in [home_city, visiting_city] if city != "Los Angeles"][0]
            non_la_team = espn_city_team_map[non_la_city]
            sbsc_game = [
                _game
                for _game in sbsc_games
                if non_la_team in [_game.contestants[0].team.name, _game.contestants[1].team.name]
            ][0]
            la_team = [
                _contestant
                for _contestant in sbsc_game.contestants
                if _contestant.team.name != non_la_team
            ][0].team.name
            if home_city == "Los Angeles":
                home_team = la_team
                visiting_team = non_la_team
            else:
                home_team = non_la_team
                visiting_team = la_team
        else:
            sbsc_game = [
                _game
                for _game in sbsc_games
                if home_city
                in [_game.contestants[0].team.espn_city, _game.contestants[1].team.espn_city]
            ][0]
            home_team = espn_city_team_map[home_city]
            visiting_team = espn_city_team_map[visiting_city]
        # Now we have all the info we need. Teams and their matching scores, plug the status of the
        # matchup. Note that we don't care about ESPN's declaration of home/visitor at this point.
        # It's just used to link a team with a score.
        score = ScoresFetch(
            team_scores={home_team: home_team_score, visiting_team: visiting_team_score},
            status=status,
        )
        ordered_scores.append(score)
        ordered_sbsc_games.append(sbsc_game)
    return ordered_sbsc_games, ordered_scores


def commit_scores(verify: bool = True) -> bool:
    """Wrapper for all score committing functionality. The logic for is-offseason and
    is-score-day is checked here, since that's fundamental to all cases of
    scoring. The other case of "did the user request an old week/season" is
    handled in the view logic, naturally.

    :param verify: Whether or not to check if there's an active game, skipping if not.
    :return: Whether or not scores were committed.
    """
    # Check if you're in a regular season week.
    season, week = queries.get_current_season_and_week()
    if verify is True and (season is None or week is None):
        logger.info(f"Not committing scores - {season=}, {week=}")
        return False
    if season is None or week is None:
        sbsc_games = queries.get_games()
    else:
        sbsc_games = queries.get_games_in_week(season, week)
    # Check if you're near the kickoff times specified by the lines, or if any game's last fetch
    # had an inprogress status.
    if verify is True:
        around_kickoff = is_around_kickoff(datetimes=[game.datetime for game in sbsc_games])
        any_inprogress = any(game.status.inprogress for game in sbsc_games)
        # If EITHER is True, fetch/commit scores again (exit only if both are False).
        if around_kickoff is False and any_inprogress is False:
            logger.info(f"Not committing scores - {around_kickoff=}, {any_inprogress=}")
            return False
    # If you've made it this far, we're clear to fetch and commit scores.
    ordered_games, ordered_scores = fetch_scores(sbsc_games=sbsc_games)
    commits.write_scores(games=ordered_games, scores=ordered_scores)
    return True
