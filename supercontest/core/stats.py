"""Handles the building of all plots on the statistics page. Queries the raw data, reformats, and
encodes the json for plotly before passing it to the frontend js to render.

Remember that the statistical analysis works by looping over every game (both teams) and then
creating a dictionary of COUNTS for something happening. Eg vikings were the favorite, eagles were
the home team, and vikings covered. Coverage would be the value, the count is +1, and the dimension
for "by" or "per" may be prediction or location. You can slice however you'd like.

Dimensions for statistical analysis are divided into multiple categories:

* 1: This is the toplevel statistic. The thing we care about measuring. It's coverage. The thing the
  user is searching for as a decisionmaker. It must be grouped by an identity from class 2.
* 2: These are the primary dimensions along which the toplevel statistic is analyzed. They're
  identities. "X" covered / did not cover. They can also be compared with one another (for
  stats that are not coverage based, but still interesting).
* 3: These are secondary dimensions that cannot directly classify coverage. They're not identities.
  But once coverage is grouped with an identity, it can be compared against these. They can also be
  compared against one another. Example: A line cannot cover, but "favorite by line" makes sense.
* 4: These are resultant dimensions. They can't be the used as the X/rows dimension (as in Y by X,
  you can't group by them), but they can be used as Y/cols. Example: Location by Margin does not
  make sense, but Margin by Location does.

"""
from typing import Callable, Iterable, cast
from collections import defaultdict
import statistics
from supercontest.dbsession import queries
from supercontest.util.plotting import form_hbar
from supercontest.models import Contestant, Game

#: Colors used for 3-item datasets, matching the green/yellow/red of bootstrap's
#: ``table-success``, ``table-warning``, and ``table-danger``.
COLOR_TRIO = ("#c3e6cb", "#ffeeba", "#f5c6cb")

# Simply alias for the types a dimension value can be.
StatsVar = int | float | str

#: The relationships between dimensions. Essentially what a variable can be compared against.
ADJ_MATRIX = {
    1: [2],
    2: [2, 3],
    3: [2, 3],
    4: [2, 3],
}


class StatsDim:  # pylint: disable=too-few-public-methods,too-many-instance-attributes
    """Our database has dimensions we care to analyze for stats. These are their properties."""

    def __init__(  # pylint: disable=too-many-arguments
        self,
        name: str,
        category: int,
        scope: str,
        val_getter: Callable[[Contestant], StatsVar],
        val_sort_key: Callable[[StatsVar], StatsVar],
        main_agg: Callable[[Iterable[StatsVar]], int | float] | None = None,
        main_plot_sort_reverse: bool = True,
        main_colors: tuple[str, ...] | None = None,
        by_val_getter: Callable[[Contestant], StatsVar] | None = None,
    ) -> None:
        """Adds all inputs as class properties.

        :param name: The name of this dimension.
        :param category: The category of the dimension. See module docstring.
        :param scope: The scope of the dimension, sbsc (this app) or nfl (global/public).
        :param val_getter: How to get the values of this dimension.
        :param val_sort_key: How to sort the values of this dimension.
        :param main_agg: The aggregation to reduce n traces into 1 trace, when main dimension.
        :param main_plot_sort_reverse: For sorting rows by trace vals, when main dimension.
        :param main_colors: The colors to display (as traces) for this, when main dimension.
        :param by_val_getter: If you want to fetch a different value for this dimension when it's
            the "by" (y axis) rather than "main" (x axis).
        """
        self.name = name
        self.category = category
        self.scope = scope
        self.val_getter = val_getter
        self.val_sort_key = val_sort_key
        self.main_agg = main_agg
        self.main_plot_sort_reverse = main_plot_sort_reverse
        self.main_colors = main_colors
        self.by_val_getter = by_val_getter


#: All dimension definitions for statistical analysis. This is ordered.
DIMENSIONS = [
    StatsDim(
        name="coverage",
        category=1,
        scope="nfl",
        val_getter=lambda contestant: contestant.coverage.name,
        val_sort_key=lambda name: ["Cover", "Push", "Noncover"].index(cast(str, name)),
        main_colors=COLOR_TRIO,
    ),
    StatsDim(
        name="prediction",
        category=2,
        scope="nfl",
        val_getter=lambda contestant: contestant.prediction.name,
        val_sort_key=lambda name: ["Favorite", "Pickem", "Underdog"].index(cast(str, name)),
    ),
    StatsDim(
        name="location",
        category=2,
        scope="nfl",
        val_getter=lambda contestant: contestant.location.name,
        val_sort_key=lambda name: ["Home", "Neutral", "Visitor"].index(cast(str, name)),
    ),
    StatsDim(
        name="team",
        category=2,
        scope="nfl",
        val_getter=lambda contestant: contestant.team.name,
        val_sort_key=str,
    ),
    StatsDim(
        name="line",
        category=3,
        scope="nfl",
        val_getter=lambda contestant: contestant.get_line(),
        val_sort_key=float,
        main_agg=statistics.fmean,  # type: ignore
        main_plot_sort_reverse=False,
        by_val_getter=lambda contestant: contestant.get_line_bucket(),
    ),
    StatsDim(
        name="dayofweek",
        category=3,
        scope="nfl",
        val_getter=lambda contestant: contestant.game.get_dow(),
        val_sort_key=lambda d: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"].index(
            cast(str, d)
        ),
    ),
    StatsDim(
        name="margin",
        category=4,
        scope="nfl",
        val_getter=lambda contestant: contestant.margin,
        val_sort_key=float,
        main_agg=statistics.fmean,  # type: ignore
    ),
    StatsDim(
        name="picks",
        category=4,
        scope="sbsc",
        val_getter=lambda contestant: len(contestant.picks),
        val_sort_key=lambda _: 0,
        main_agg=sum,  # type: ignore
    ),
]


def build_plots(  # pylint: disable=too-many-locals
    games: list[Game],
    main_dim: StatsDim,
    by_dims: list[StatsDim],
    per_dim: StatsDim | None = None,
) -> list[str]:
    """Takes the dimensions that a view cares about, and constructs the data + plots.

    The "main" dimension sets the cols on the hbar, the xaxis vals.
    The "by" list creates a plot for each item, main dimension against this as rows, the yaxis vals.
    The "per" is multivariate - a group of "main by x" plots for each value along this dimension.

    Therefore the main dimension is a dict instead of defaultdict. It's bounded, and we need to
    show all data, even if zero (else we might have 1 bar instead of stacked bars). The "per"
    dimension is also bounded ahead of time. The "by" dimensions are not; we pull whatever values
    are in the db in realtime and show those ranges.

    Objects which require DB queries are fetched before iteration.

    This function works by simply iterating over all the raw data and building COUNTS based on keys
    across all the dimensions. Then the plotter will default to calculate percentage-of-total from
    those counts, or ``agg`` can flatten into an average, etc.

    Loops over all games (including both contestants) to get the data. This could be genericized,
    defined alongside that dimension. It's a param so that multiple plot builders can be passed
    the same iterable instead of requerying.

    :param games: List of games to iterate over.
    :param main_dim: The primary dimension to analyze (eg coverage).
    :param by_dims: The list of dimensions to slice the primary dimension by (eg coverage by team).
    :param per_dim: The multivariate dimension to repeate the above analysis for (eg coverage by
        team by prediction, so coverage by team when favorite and coverage by team when pickem and
        coverage by team when underdog).
    :return: List of json-encoded plot strings.
    """
    if per_dim is None:
        # The null dimension for (univariate) statistical analysis.
        per_dim = StatsDim(
            name="null",
            category=100,  # arbitrarily high
            scope="null",
            val_getter=lambda _: "null",
            val_sort_key=lambda _: 0,
        )
    main_vals: set[StatsVar] = set()
    per_vals: set[StatsVar] = set()
    data: defaultdict[StatsVar, defaultdict[str, defaultdict[StatsVar, defaultdict[StatsVar, int]]]]
    data = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(int))))
    for game in games:
        for contestant in game.contestants:
            for by_dim in by_dims:
                per_val = per_dim.val_getter(contestant)
                by_val_getter = by_dim.by_val_getter or by_dim.val_getter
                by_val = by_val_getter(contestant)
                main_val = main_dim.val_getter(contestant)
                main_vals.add(main_val)
                per_vals.add(per_val)
                data[per_val][by_dim.name][by_val][main_val] += 1
    sorted_main_vals = tuple(sorted(main_vals, key=main_dim.val_sort_key))
    sorted_per_vals = tuple(sorted(per_vals, key=per_dim.val_sort_key))
    plots: list[str] = []
    for per_val in sorted_per_vals:
        for by_dim in by_dims:
            title = f"{main_dim.name.capitalize()} By {by_dim.name.capitalize()}"
            if per_dim.name.lower() != "null":
                title += f", {per_dim.name.capitalize()} = {per_val}"
            traces = data[per_val][by_dim.name]
            plots.append(
                form_hbar(
                    traces=traces,
                    title=title,
                    trace_order=sorted_main_vals,
                    agg=main_dim.main_agg,
                    colors=main_dim.main_colors,
                    reverse=main_dim.main_plot_sort_reverse,
                )
            )
    return plots


def build_univariate_plots(
    games: list[Game],
    dims: list[StatsDim],
    only: list[StatsDim] | None = None,
) -> list[str]:
    """Compares all combinations of dimensions that are compatible 1:1.

    :param games: List of games to iterate over.
    :param dims: All dimensions to consider.
    :param only: To restrict the main dimension.
    :return: List of json-encoded plot strings.
    """
    plots: list[str] = []
    for main_dim in only or dims:
        plots += build_plots(
            games=games,
            main_dim=main_dim,
            by_dims=[
                dim
                for dim in dims
                if dim.category in ADJ_MATRIX[main_dim.category] and dim != main_dim
            ],
        )
    return plots


def build_bivariate_plots(games: list[Game], dims: list[StatsDim], join_to: int = 1) -> list[str]:
    """Lock an identity (category 2) to each value, then compare the joined (primary dimenion +
    identity) to the other dimensions.

    :param games: List of games to iterate over.
    :param dims: All dimensions to consider.
    :param join_to: The primary dimension category to join an identity with. Shouldn't change.
    :return: List of json-encoded plot strings.
    """
    plots: list[str] = []
    for per_dim in [dim for dim in dims if dim.category in ADJ_MATRIX[join_to]]:
        for main_dim in [dim for dim in dims if dim.category == join_to]:
            plots += build_plots(
                games=games,
                main_dim=main_dim,
                by_dims=[
                    dim
                    for dim in dims
                    if dim.category in ADJ_MATRIX[per_dim.category] and dim != per_dim
                ],
                per_dim=per_dim,
            )
    return plots


def build_nfl_plots(season: int | None) -> tuple[list[str], int]:
    """The plot builder.

    :param season: The season to restrict the scope to.
    :return: List of json-encoded plot strings, and total number of games in scope.
    """
    dims = [dim for dim in DIMENSIONS if dim.scope == "nfl"]
    if season is None:
        games = queries.get_games()
    else:
        games = queries.get_finished_games_in_season(season=season)
    plots = build_univariate_plots(games=games, dims=dims)
    plots += build_bivariate_plots(games=games, dims=dims)
    return plots, len(games)


def build_sbsc_plots(season: int | None) -> tuple[list[str], int]:
    """The plot builder.

    :param season: The season to restrict the scope to.
    :return: List of json-encoded plot strings, and total number of games in scope.
    """
    sbsc_dims = [dim for dim in DIMENSIONS if dim.category == 4 and dim.scope == "sbsc"]
    if season is None:
        games = queries.get_games()
    else:
        games = queries.get_finished_games_in_season(season=season)
    return build_univariate_plots(games=games, dims=DIMENSIONS, only=sbsc_dims), len(games)
