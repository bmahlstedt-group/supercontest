"""Logic to handle all user picks."""
import os
import time
import logging
from typing import cast
from collections import defaultdict
from pathlib import Path
from flask import current_app, url_for
from selenium import webdriver
from selenium.webdriver.common.by import By
from supercontest.dbsession import queries, commits
from supercontest.comms import email
from supercontest.util.web import with_webdriver
from supercontest.util.exceptions import InvalidPicks

logger = logging.getLogger(__name__)


def commit_picks(
    user_id: int,
    season: int,
    week: int,
    teams: list[str],
    emails: list[str] | None = None,
    verify: bool = True,
) -> None:
    """Wrapper to write picks to the database.

    :param user_id: The ID of the user submitting picks.
    :param season: The season to pick for.
    :param week: The week to pick for.
    :param teams: The picks from the client.
    :param emails: The email addresses to send confirmation to.
    :param verify: Whether or not to check conditions like max5, which weekday, etc.
    :raises InvalidPicks: If verifications fail.
    """
    # Global validity checks.
    if verify is True:
        if not queries.is_regseason():
            raise InvalidPicks("Picks cannot be placed outside of the regular season.")
        if not queries.are_picks_open():
            raise InvalidPicks("Picks can only be placed Wednesday-Saturday.")
        if len(teams) > 5:
            raise InvalidPicks("You cannot select more than 5 teams per week.")
        if len(teams) != len(set(teams)):
            raise InvalidPicks("A team cannot be selected more than once in the same week.")
    teams = [team for team in teams if team]  # strip pick=""
    # Check their existing picks (if any) before comparing to this new attempt. Use sets so we can
    # do intersections and such. Explictly use ``set`` functions instead of set literals ``{}`` in
    # case any of the lists are empty.
    old_pick_objs = set(
        queries.get_picks_in_week_for_user(season=season, week=week, user_id=user_id)
    )
    # Find the existing pick objects to leave alone, based on matching team names with new picks.
    old_pick_objs_keep = set(pick for pick in old_pick_objs if pick.contestant.team.name in teams)
    # Now we have the list of actual NEW picked teams to be upserted.
    new_pick_teams = set(teams) - set(pick.contestant.team.name for pick in old_pick_objs_keep)
    # Map the new picked team names to the game and team objects that they'll correspond with.
    # We need this for both validity checks as well as the subsequent ``Pick`` instantiation.
    new_pick_contestant_objs = [
        queries.get_contestant_from_team(season=season, week=week, team=team)
        for team in new_pick_teams
    ]
    # Specific validity checks.
    if verify is True:
        for new_pick_contestant_obj in new_pick_contestant_objs:
            if (opponent := new_pick_contestant_obj.get_opponent().team.name) in teams:
                raise InvalidPicks(
                    f"You tried to pick {new_pick_contestant_obj.team.name} "
                    f"but you already picked the opponent: {opponent}"
                )
            if new_pick_contestant_obj.game.status.unstarted is False:
                raise InvalidPicks("Picks cannot be place for games that have already started.")
    # Validity checks are now done. The ``new_pick_teams/contestant_objs`` are good.
    # The logic to (update vs delete vs insert) is in the called function.
    commits.write_picks(
        old_pick_objs_update=list(old_pick_objs - old_pick_objs_keep),
        new_pick_contestant_objs=new_pick_contestant_objs,
        user_id=user_id,
    )
    if emails:
        email.email_picks(season=season, week=week, teams=list(new_pick_teams), emails=emails)


def email_unfinished_pickers() -> bool:
    """Helper function to query the db and find the people who have not submitted
    all 5 picks for the week, then email them a reminder.

    :return: Whether or not the email was sent.
    """
    if not queries.is_regseason():
        logger.info("Not sending unfinishedPickers email, it is not an active week")
        return False
    logger.info("Determining who has not submitted the full 5 picks")
    current_season, current_week = queries.get_current_season_and_week()
    # Get all users. The notification setting is checked in email_picks_closing().
    all_users = queries.get_all_user_emails()
    # Query for <5 picks.
    all_picks = []
    if current_season is not None and current_week is not None:
        all_picks = queries.get_picks_in_week(season=current_season, week=current_week)
    pick_counts_by_user: defaultdict[str, int] = defaultdict(int)
    for pick in all_picks:
        pick_counts_by_user[pick.user.email] += 1
    finished_pickers: list[str] = [
        email for email, pick_count in pick_counts_by_user.items() if pick_count == 5
    ]
    unfinished_pickers = list(set(all_users) - set(finished_pickers))
    logger.info(f"Found: {unfinished_pickers}")
    email.email_picks_closing(
        season=current_season,
        week=current_week,
        unfinished_pickers=unfinished_pickers,
    )
    return True


@with_webdriver
def screenshot_picks_table(
    driver: webdriver.Chrome,
    season: int | None = None,
    week: int | None = None,
) -> tuple[int, int, str]:
    """Grabs a screenshot of the all-picks table, to be emailed to users.
    Does not filter the table by Paid league. Grabs the whole Free league for
    the current season/week. Always pulls from the production app (domain).

    This function assumes you're in an active regular season week. The check
    is completed by the caller, not this function.

    :param driver: The chrome webdriver, always passed by the decorator.
    :param season: Exposed for testing, if you want to screenshot a picks table outside of the
        current season+week. You must pass both season+week to trigger this.
    :param week: Exposed for testing, if you want to screenshot a picks table outside of the
        current season+week. You must pass both season+week to trigger this.
    :return: Season year, if in an active season.
    :return: Week number, if in an active week.
    :return: Relative path to the output ``.png`` file.
    :raises ValueError: If not in a current season+week, and not passed a season+week instead.
    """
    # If the user passed season/week values, use them. Else current week/season.
    # If no passed values, and no current values, error.
    current_season, current_week = queries.get_current_season_and_week()
    season = season or current_season
    week = week or current_week
    if season is None or week is None:
        raise ValueError("Not in a current week/season, and you didn't pass another week/season.")
    url = url_for("picks.picks", season=season, league=0, week=week, _external=True)
    driver.get(url)
    # Auth to see table.
    email_input = driver.find_element(By.ID, "email")
    password_input = driver.find_element(By.ID, "password")
    email_input.send_keys(cast(str, current_app.config["APP_ADDRESS"]))
    password_input.send_keys(cast(str, current_app.config["APP_PASSWORD"]))
    password_input.submit()  # selenium walks up the dom and finds submit button
    time.sleep(1)
    # Screenshot table.
    width = 2200  # reactive axis, but empirically found for good/full aesthetic
    ybuffer = 30
    savedir = os.path.join(os.sep, "sc", "screenshot_archive")
    # Create the dir if doesn't exist already
    Path(savedir).mkdir(parents=True, exist_ok=True)
    filename = os.path.join(savedir, f"{season}-{week}.png")
    driver.get(url)
    table = driver.find_element(By.ID, "allPicksTable")
    height: int = table.location["y"] + table.size["height"] + ybuffer
    driver.set_window_size(width, height)
    driver.save_screenshot(filename)
    return season, week, filename


def email_all_picks() -> bool:
    """Helper function to get the screenshot of the all-picks table and email it to the users that
    want it. This function handles the subscription check.

    :return: Whether or not the email was sent.
    """
    if not queries.is_regseason():
        logger.info("Not sending allPicks email, it is not an active week")
        return False
    logger.info("Now that lockdown has passed, grabbing a screenshot of the table")
    season, week, filename = screenshot_picks_table()  # pylint: disable=no-value-for-parameter
    logger.info(f"Saved screenshot at {filename}")
    logger.info("Now emailing users who have the notification enabled")
    email.email_all_picks(season=season, week=week, png=filename)
    return True
