"""Logic for fetching and parsing matchup lines."""
import re
import logging
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from supercontest.comms.email import email_picks_open
from supercontest.dbsession import commits, queries
from supercontest.util.web import with_webdriver
from supercontest.util.timing import convert_westgate_datetime_to_utc, get_utc_now
from supercontest.util.types import LinesFetch

logger = logging.getLogger(__name__)


def fetch_lines() -> list[LinesFetch]:
    """Hits the official Westgate site and returns its line table as an html string,
    then coerces it into the proper format. Uses requests and BeautifulSoup.

    :return: The fetched line data.
    :raises ValueError: If called when not in a current week (no lines to fetch).
    """
    url = "https://www.westgateresorts.com/hotels/nevada/las-vegas/westgate-las-vegas-resort-casino/supercontest-weekly-card/"
    resp = requests.get(url)
    html = resp.text
    cleaned_html = re.sub(r'</?p.*?>', '', html)
    soup = BeautifulSoup(cleaned_html, 'html.parser')
    table = soup.find('table')
    rows = table.find_all('tr')
    date = ""
    lines: list[LinesFetch] = []
    for row in rows:
        cells = row.find_all(['td', 'th'])
        data = [cell.get_text(strip=True) for cell in cells if cell.text != ""]
        if 'TEAM' in data and 'TIME' in data:
            continue
        if len(data) == 1:
            date = data[0].replace("PRO FOOTBALL - ", "")
        elif len(data) == 4:
            favored_team, raw_time, underdog_team, line = data
            if raw_time == "TBA":
                raw_time = "1:05 PM"
            date_time = date + " " + raw_time[0 : raw_time.index("M") + 1]
            line = line.replace("PK", "+0").replace("+", "").replace("½", ".5").replace("!", "").replace(",", ".")
            favored_team = favored_team.split(None, 1)[-1].strip().replace("'", "")
            underdog_team = underdog_team.split(None, 1)[-1].strip().replace("'", "")
            if underdog_team == 'BELGALS':
                underdog_team = 'BENGALS'
            if "*" in favored_team:
                favored_team = favored_team.replace("*", "").strip()
                home_team = favored_team
            elif "*" in underdog_team:
                underdog_team = underdog_team.replace("*", "").strip()
                home_team = underdog_team
            else:
                home_team = None
            lines.append(
                LinesFetch(
                    datetime=date_time,
                    line=line,
                    favored_team=favored_team,
                    underdog_team=underdog_team,
                    home_team=home_team,
                )
            )
        else:
            continue
    return lines


@with_webdriver
def fetch_lines_selenium(  # pylint: disable=too-many-locals
    driver: webdriver.Chrome,
    week: int = 0,
) -> list[LinesFetch]:
    """Hits the official Westgate site and returns its line table as an html string,
    then coerces it into the proper format.

    Note the driver arg is ALWAYS passed by the decorator, which injects and
    changes the function signature. This messes with linters/typecheckers,
    so we simply default it to a (never used) chromedriver instance.

    :param driver: The chrome webdriver, always passed by the decorator.
    :param week: Exposed for testing, if you want to fetch lines from a specific week.
    :return: The fetched line data.
    :raises ValueError: If called when not in a current week (no lines to fetch).
    """
    if (current_week := queries.get_current_week()) is None:
        raise ValueError("Not in a current week")
    week = week or current_week
    driver.get(f"https://www.superbook.com/supercontest/2023-weekly-card-{week}")
    table = driver.find_element(By.CSS_SELECTOR, "table")
    rows = table.find_elements(By.CSS_SELECTOR, "tr")
    # Extract the text into Python objects
    date = ""
    lines: list[LinesFetch] = []
    for row in rows:
        cells = row.find_elements(By.CSS_SELECTOR, "td")
        data = [cell.text for cell in cells if cell.text != ""]
        # If only the first cell is populated, it's just a date row,
        # which is concatenated for subsequent iterations until it changes.
        if len(data) == 1:
            # Superbook randomly started prefixing lines with this. Strip it.
            date = data[0].replace("PRO FOOTBALL - ", "")
        # If the row has 4 values, it's a line - extract the info.
        elif len(data) == 4:
            favored_team, raw_time, underdog_team, line = data
            if raw_time == "TBA":  # coronavirus broke so many things
                raw_time = "1:05 PM"
            # Superbook started adding dumb stuff like "6:30 AM London, UK"
            # You just want "6:30 AM" so strip everything after the M.
            date_time = date + " " + raw_time[0 : raw_time.index("M") + 1]
            # Switch push to 0 in the line, remove the + (always pos for first col), and decimal.
            line = line.replace("PK", "+0").replace("+", "").replace("½", ".5").replace("!", "")
            # Remove the team number and whitespace, we only care about name.
            favored_team = favored_team.split(None, 1)[-1].strip().replace("'", "")
            underdog_team = underdog_team.split(None, 1)[-1].strip().replace("'", "")
            # Calculate home team and strip asterisk. 2023 Week 12 had a bug where westgate
            # posted a space between team and *. Remove those too.
            if "*" in favored_team:
                favored_team = favored_team.replace("*", "").strip()
                home_team = favored_team
            elif "*" in underdog_team:
                underdog_team = underdog_team.replace("*", "").strip()
                home_team = underdog_team
            else:
                home_team = None
            # Now we have all the teams. We don't need to worry about casing, the team IDs will
            # be looked up with an ilike before writing to the DB.
            lines.append(
                LinesFetch(
                    datetime=date_time,
                    line=line,
                    favored_team=favored_team,
                    underdog_team=underdog_team,
                    home_team=home_team,
                )
            )
        # sometimes westgate will return empty rows in the table - ignore them
        else:
            continue
    return lines


def commit_lines(verify: bool = True) -> bool:
    """Wrapper for all line committing functionality.

    :param verify: Checks if lines are already in the db and if datetimes are in the future.
    :return: Whether or not lines were committed.
    :raises ValueError: If the fetched lines are empty or have old dates.
    """
    # Check if the lines are already in the db for the week you're currently in
    # (make it idempotent).
    season, week = queries.get_current_season_and_week()
    if verify:
        if season is None or week is None:
            logger.warning("Not committing lines, not in an active week.")
            return False
        if queries.are_games_in_db(season=season, week=week):
            logger.warning(f"Games for {season} week {week} are already in the db.")
            return False
    # If they aren't, continue.
    lines = fetch_lines()  # pylint: disable=no-value-for-parameter
    # Error early if lines are empty.
    if not lines:
        raise ValueError("The fetched lines are empty. Something is wrong with the scrape.")
    datetimes = [convert_westgate_datetime_to_utc(line["datetime"]) for line in lines]
    utc_now = get_utc_now()
    # If a single line returns with a datetime for an old matchup (one before
    # the moment this function is called), reject them.
    if verify and any((datetime < utc_now for datetime in datetimes)):
        raise ValueError("The line fetcher is receiving old lines from Westgate.")
    # If you made it this far, you got proper lines for the upcoming week.
    week_id = queries.get_week_id(season=season, week=week)
    logger.info("Committing lines")
    commits.insert_contestants(week_id=week_id, linesfetch=lines)
    # Email the users who have this notification set.
    logger.info("Emailing users")
    email_picks_open(season=season, week=week)
    return True
