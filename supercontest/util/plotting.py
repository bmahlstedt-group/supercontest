"""Utilities for graphing data."""
from typing import Any, Callable, Iterable
from collections import defaultdict
from itertools import accumulate
from statistics import fmean
import plotly


def form_hbar(  # pylint: disable=too-many-arguments,too-many-locals
    traces: defaultdict[int | float | str, defaultdict[int | float | str, int]],
    title: str,
    trace_order: tuple[int | float | str, ...],
    reverse: bool = True,
    colors: tuple[str, ...] | None = None,
    counts_to_perc: bool = True,
    agg: Callable[[Iterable[int | float | str]], int | float] | None = None,
) -> str:
    """Plots a horizontal bar chart of the given data.

    :param traces: ``{row: trace: count}``. The keys are the rows (the y-axis values of the hbar),
        common across all traces. The values of the traces dict are dicts themselves, with a key for
        each trace (eg cover) and value for each count (eg 251 games covered). If there's more than
        one trace, it will stack them (in order of ``names``).
    :param title: Title of plot.
    :param trace_order: For any plot, the rows must be sorted. This is the official "ranking" - the
        conclusion of the plot. It will sort by the values of the traces. Trace order is given by
        this arg, so the value of the first trace name will be the primary sort, then the
        second item will define the trace used to extract the values for secondary
        sort, and on and on.
    :param reverse: Sorting the rows by trace values. By default, python sorts low to high.
        This function defaults to ``True``, sorting high to low.
    :param colors: Ordered colors of each trace (values).
    :param counts_to_perc: By default, the innermost values are assumed to be counts and converted
        to per-row percentages (accross all traces). If ``False``, this leaves them as counts. If
        you're only passing one trace, this will coerce to ``False`` since the percentage will be
        100 for all rows.
    :param agg: A function to aggregate the innermost values across all traces. This will flatten a
        stacked hbar of many traces into a single trace of the aggregated value (per row). This
        implicitly sets ``counts_to_perc=False``, since you're reducing dimensionality to 1.
        Obviously the aggregation function must be compatible with the value types: mean() works for
        numbers, but not strings.
    :return: The json dump of the plot.
    """
    # We're aggregating or converting to percentages, so allow floats instead of just int counts.
    final_traces: Any = traces.copy()
    if agg is not None:
        name = agg.__name__
        trace_order = (name,)
        title = f"({agg.__name__}) {title}"
        for row, single_row_traces in traces.items():
            agg_val = agg(val for val, count in single_row_traces.items() for _ in range(count))
            final_traces[row] = {name: agg_val}
    if len(trace_order) == 1:
        counts_to_perc = False
    if counts_to_perc is True:
        title = "(%) " + title
        for row, single_row_traces in traces.items():
            total = sum(single_row_traces.values())
            for trace_val, trace_count in single_row_traces.items():
                final_traces[row][trace_val] = trace_count / total * 100
    sorted_rows = sorted(
        final_traces.keys(),
        key=lambda key: tuple(final_traces[key][trace_val] for trace_val in trace_order),
        reverse=reverse,
    )
    axis_kwargs = {"showgrid": True, "gridcolor": "#eee"}
    layout = plotly.graph_objs.Layout(
        xaxis=axis_kwargs | {"side": "top"},
        # Even for numbers we specify category so that plotly doesn't infer and reorder. This
        # also tells plotly to display every value.
        yaxis=axis_kwargs | {"type": "category"},
        title={"text": title, "x": 0.5, "xanchor": "center"},
        showlegend=False,
        barmode="stack" if len(trace_order) > 1 else "group",
        plot_bgcolor="rgba(0,0,0,0)",
        hovermode="y",
        height=15 * len(sorted_rows) + 200,  # bar width, and an allocation for title space
    )
    fig = plotly.graph_objs.Figure(layout=layout)
    vline = 0
    # Plot. This is done trace by trace (not row by row).
    for index, trace in enumerate(trace_order):  # not reversed; left-to-right is correct
        sorted_values = [final_traces[row][trace] for row in sorted_rows]
        # Plot a vertical line at the average of the data for this trace. Remember that traces
        # can be categorical (cover, push, noncover), but the values will always be numerical
        # (counts, or percentages).
        mean = fmean(sorted_values)
        vline += mean
        fig.add_vline(x=vline)
        trace_kwargs: dict[str, Any] = {
            # Plotly will plot the traces starting from the origin, so for a horizontal bar chart,
            # the plots go in order of bottom-to-top. We want the best on the top. So this has
            # nothing to do with the ``reverse`` arg here, it's just to invert for plotly's
            # horizontal bar alignment (always).
            "y": list(reversed(sorted_rows)),
            "x": list(reversed(sorted_values)),
            "orientation": "h",
            "name": trace,
        }
        if colors:
            trace_kwargs["marker"] = {"color": colors[index]}
        fig.add_trace(plotly.graph_objs.Bar(**trace_kwargs))
    return fig.to_json()


def form_scatter(
    traces: tuple[dict[int | float, int | float], ...],
    title: str,
    names: tuple[str, ...],
    visibilities: tuple[bool, ...],
    pad_zero: bool = True,
    accumulate_y: bool = True,
) -> str:
    """Plots a scatter chart of the given data.

    :param traces: Each element of the tuple is a trace, structured as a dict where the keys = xaxis
        and the vals = yaxis. This function only supports numerical data (for now), so it sorts
        according to those values (origin is 0/0, increasing along both axes).
    :param title: Title of plot.
    :param names: Ordered names of the traces (values).
    :param visibilities: Ordered settings for the traces, True = show by default, False = legend.
    :param pad_zero: Inject an x,y point of 0,0 at the beginning of each scatter's trace.
    :param accumulate_y: To accumulate yaxis values or not.
    :return: The json dump of the plot.
    """
    axis_kwargs = {"showgrid": True, "gridcolor": "#eee"}
    layout = plotly.graph_objs.Layout(
        xaxis=axis_kwargs,
        yaxis=axis_kwargs,
        title={"text": title, "x": 0.5, "xanchor": "center"},
        plot_bgcolor="rgba(0,0,0,0)",
    )
    fig = plotly.graph_objs.Figure(layout=layout)
    for index, trace in enumerate(traces):
        # This is deterministic (same as insertion order, as of py3.6).
        xvals: list[int | float] = list(trace.keys())
        yvals: list[int | float] = list(trace.values())
        if pad_zero is True:
            xvals = [0] + xvals
            yvals = [0] + yvals
        if accumulate_y is True:
            yvals = list(accumulate(yvals))
        trace_kwargs: dict[str, Any] = {
            "x": xvals,
            "y": yvals,
            "name": names[index],
            "visible": True if visibilities[index] else "legendonly",
        }
        fig.add_trace(plotly.graph_objs.Scatter(**trace_kwargs))
    return fig.to_json()
