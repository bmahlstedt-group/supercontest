"""Utilities to assist with logging for the entire application."""

import logging
from typing import Any, Iterable, cast
from operator import attrgetter

# Named differently in this module because the variable "logger" is used so
# many times below. I just want to avoid confusion.
module_logger = logging.getLogger(__name__)


def create_handler() -> logging.Handler:
    """Creates the standard streamhandler, with supercontest-specific format.
    Then attach it to any logger.
    Example: ``logging.getLogger("mylogger").addHandler(create_handler())``

    :return: A formatted stream handler.
    """
    streamhandler = logging.StreamHandler()
    formatter = logging.Formatter(
        fmt="[%(asctime)s] [%(name)30s] [%(levelname)s] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S %z",
    )
    streamhandler.setFormatter(formatter)
    return streamhandler


def print_data(data: list[dict[str, Any]], keys: list[str], widths: list[int]):
    """Takes a list of data and prints the desired keys in columns as defined by the input widths.
    Separates each column by a space, and surrounds each column with brackets. This intentionally
    prints to stdout, rather than a custom logger.

    :param data: The data to print, each item a row, in order.
    :param keys: The keys to print as cols, in order.
    :param widths: The width of the cols to print, in order.
    """
    for row in data:
        cols = [f"[{str(row[key]).ljust(width)}]" for key, width in zip(keys, widths)]
        print(" ".join(cols))


def show_loggers(attrs: list[str] | None = None):
    """Helpful printer to show the tree of loggers in the current app. Will pretty-print them to
    stdout alphabetically, with the desired attributes. Some rules:

    * If the attr doesn't exist for a logger, it will print None.
    * If the attr is a list, it will print a joined string of the items.
    * If the attr is "level", it will cast to the logger's name (not number, the default).
    * If the attr is anything else, it will just ``str()`` the attr value.

    :param attrs: The columns to print for each logger. Defaults to ``name``, ``level``,
        ``parent``, ``handlers``, ``propagate``.
    """
    module_logger.info("All current logger information is printed below:")
    # Define the information you want printed.
    attrs = [
        "name",
        "level",
        "parent",
        "handlers",
        "propagate",
    ]
    # Add the root logger into the dict of all nonroot loggers.
    all_loggers_dict = logging.root.manager.loggerDict | {"root": logging.getLogger()}
    # Strip placeholders (modules that don't have loggers).
    all_loggers_list = [
        logger
        for logger in all_loggers_dict.values()
        if not isinstance(logger, logging.PlaceHolder)
    ]
    # Sort alphabetically.
    sorted_loggers_all_info = sorted(all_loggers_list, key=attrgetter("name"))
    # Strip the key/val pairs we don't need, and cast obj with attrs to dict
    sorted_loggers = [
        {attr: getattr(logger, attr, None) for attr in attrs} for logger in sorted_loggers_all_info
    ]
    # Iterate through and cast level numbers to level names, if needed.
    if "level" in attrs:
        for logger in sorted_loggers:
            level = logger["level"]
            level = level if level is not None else 0
            # pylint: disable=protected-access
            logger["level"] = logging._levelToName[level]  # pyright: ignore[reportPrivateUsage]
            # pylint: enable=protected-access
    # Iterate through and extract the name from the parent class, if needed.
    # eg "<RootLogger root (WARNING)>" -> "root"
    if "parent" in attrs:
        for logger in sorted_loggers:
            parent_words = str(logger["parent"]).split()
            if len(parent_words) > 1:
                logger["parent"] = parent_words[1]
    # Iterate through and join any lists into a single string.
    for logger in sorted_loggers:
        for key, val in logger.items():
            if isinstance(val, list):
                string_items = [str(item) for item in cast(Iterable[str], val)]
                logger[key] = " ".join(string_items)
    # Determine the width of cols to print. Find the max of the attribute to be
    # printed (or the column header, if that's longer). Casting to str() just in
    # case the value of attribute is an int or something without a length.
    widths = [
        max(max(len(str(logger_dict[attr])) for logger_dict in sorted_loggers), len(attr))
        for attr in attrs
    ]
    # Print headers.
    headers = [{key: f"{key.upper():-^{width}}" for key, width in zip(attrs, widths)}]
    print_data(data=headers, keys=attrs, widths=widths)
    # Iterate over each logger and show info.
    print_data(data=sorted_loggers, keys=attrs, widths=widths)
