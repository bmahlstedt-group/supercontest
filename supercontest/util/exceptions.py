"""Custom errors raised by the application."""


class InvalidPicks(ValueError):
    """User did not submit valid picks."""
