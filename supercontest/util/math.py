"""Mathematic utilities."""
from typing import TypeVar, Any
import math
from numpy import digitize
from scipy.stats import logser

T = TypeVar("T")
digitize: Any


def convert_num_to_int_if_whole(num: float | int) -> float | int:
    """Simply converts a float or int to an int if it's a whole number or leaves it as float
    if it's a float.

    :param num: The number.
    :return: The converted number.
    """
    num = float(num)  # cast to float first so we can use is_integer()
    return int(num) if num.is_integer() else num


def round_down_to_nearest_10(num: int | float) -> int:
    """Simply rounds a number down to the nearest 10 (used for payouts).

    :param num: The number.
    :return: The rounded number.
    """
    return math.floor(num / 10) * 10


def get_percentage(num: int | float, den: int | float, round_to: int = 1) -> int | float:
    """Many pieces of the results indicate percentage (like pick accuracy). This function handles
    the divide by zero case, and the formatting into percentage, and the round to tenth decimal
    place.

    :param num: The numerator.
    :param den: The denominator.
    :param round_to: Decimal places to round to (0 is integer).
    :return: The resultant percentage, or inf if divide-by-zero. (0/0 is treated as 0).
    """
    if num == 0:
        return 0
    if den == 0:
        return math.inf
    perc = 100 * num / den
    perc_rounded = round(perc, round_to)
    return convert_num_to_int_if_whole(perc_rounded)


def log_rank_distribution(
    total: int | float,
    floor: int | float,
    max_distributions: int = 100,
    shape: float = 0.9,
) -> tuple[list[int], int]:
    """Takes a total amount and divides it discretely among the top ranks. Uses a logarithmic
    distribution. The floor is where the distribution stops, with the tail of the logser (to
    infinity) just going to the first rank.

    Ranks 2-n are then rounded down to the nearest 10, with remainders
    going to 1st rank. The final round-down from 1st place is returned to the
    caller.

    :param total: The quantity to be distributed.
    :param floor: The cutoff for the minimum distribution.
    :param max_distributions: If you want to only distribute to top n ranks. Otherwise, set it
        arbitrarily high, and it will use the specified "floor" as the final rank to
        receive distribution.
    :param shape: The shape of the discrete logarithmic series (``p``). Numbers closer to 1 flatten
        the distribution. Numbers closer to zero have heavier distributions to the top ranks.
    :return: The quantities distributed to the top ranks.
    :return: Remainder from 1st rank round-down.
    """
    dist_all: list[float] = [logser.pmf(rank, shape) for rank in range(1, max_distributions + 1)]
    dist = [perc for perc in dist_all if (total * perc) > floor]
    results: list[int] = []
    for perc in dist[1:]:  # from 2nd place to last place that gets paid
        results.append(round_down_to_nearest_10(total * perc))
    first = round_down_to_nearest_10(total - sum(results))  # 1st remainder
    results.insert(0, first)
    remainder = int(total - sum(results))  # final round-down from 1st
    return results, remainder


def bucket_number(num: int | float, step: int, stop: int) -> str:
    """Bins/discretizes a number based on range. Automatically creates signed buckets (for the
    negative increments of ``step`` down to ``-stop``). Each bin is inclusive on the lower/left val.

    :param num: The value to place in a bin.
    :param step: The width of the bins.
    :param stop: The last bin. It will also create bookend bins from ``stop`` to ``inf`` and
        ``-stop`` to ``-inf``.
    :return: The bin.
    """
    bins = [-math.inf] + list(range(-stop, stop + step, step)) + [math.inf]
    index = digitize(num, bins)
    return f"{bins[index-1]} <= x < {bins[index]}"
