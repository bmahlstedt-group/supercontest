"""Factory for cross-site request forgery."""

from flask_wtf.csrf import CSRFProtect

#: The CSRF object used throughout the app to protect specific entities.
csrf = CSRFProtect()
