"""Utilities around config management."""
from typing import Any
from configparser import ConfigParser


def get_sectionless_config(path: str) -> dict[Any, Any]:
    """Takes a config file that does not have sections (headers) and returns
    the content anyway.

    The reason this function is necessary: The database config files are passed
    both (1) as ``env_file`` in docker compose and (2) as configparser inputs (ini
    style) in ``create_app()``. Env files cannot have section headers. Ini files
    must have section headers.

    So we keep the raw files sectionless. Docker reads them directly. The
    ``create_app()`` function uses this to inject fake section headers.

    Note also that because ConfigParser mimics ini, and ini is case-insensitive,
    we cast to strings here to maintain the casing in the raw files.

    :param path: Path to the config to be read.
    :return: The key/val pairs from the config.
    """
    parser = ConfigParser()
    parser_option: Any = str
    parser.optionxform = parser_option
    with open(path, encoding="utf-8") as stream:
        parser.read_string("[SECTION]\n" + stream.read())
    return dict(parser["SECTION"])
