"""The blueprint for the matchups views."""
from typing import Any
from flask import Blueprint, render_template, g
from flask_user import current_user, login_required
from supercontest.core import results
from supercontest.models.schemas import GameSchema
from supercontest.views.common import url_defaults, url_value_preprocessor

#: The blueprint for the matchups views.
matchups_blueprint = Blueprint("matchups", __name__, url_prefix="/matchups")

#: Which URL path params are required for this view.
matchups_values = {
    "league": False,
    "week": True,
}


@matchups_blueprint.url_defaults
def matchups_url_defaults(endpoint: str, values: dict[str, Any]):
    """Wrapper for common defaults call. The ``values`` dict is mutated.

    :param endpoint: The flask endpoint.
    :param values: The flask values.
    """
    url_defaults(endpoint=endpoint, values=values, **matchups_values)


@matchups_blueprint.url_value_preprocessor
def matchups_url_value_preprocessor(  # pylint: disable=unused-argument
    endpoint: str | None,
    values: dict[str, Any] | None,
):
    """Wrapper for common preprocessors call. Values are extracted then added to ``g``.

    :param endpoint: The flask endpoint.
    :param values: The flask values.
    """
    url_value_preprocessor(values=values, **matchups_values)


@matchups_blueprint.get("/season<season>/week<week>")
@login_required
def matchups() -> str:
    """The route to show all matchups for a given week, and the current user's picks.

    :return: The rendered template for the view.
    """
    sorted_games, pick_teams = results.view_matchups(
        season=g.season,
        week=g.week,
        user_id=current_user.id,
    )
    game_schema: Any = GameSchema(many=True)
    dumped_sorted_games = game_schema.dump(sorted_games)
    return render_template(
        "contest/matchups.html",
        sorted_games=sorted_games,
        dumped_sorted_games=dumped_sorted_games,
        pick_teams=pick_teams,
    )
