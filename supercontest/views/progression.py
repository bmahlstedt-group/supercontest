"""The blueprint for the progression views."""
from typing import Any
from flask import Blueprint, render_template, g
from flask_user import current_user, login_required
from supercontest.core import results
from supercontest.util.plotting import form_scatter
from supercontest.views.common import url_defaults, url_value_preprocessor, VIEW_CACHE_TTL
from supercontest.models import cache

#: The blueprint for the progression views.
progression_blueprint = Blueprint("progression", __name__, url_prefix="/progression")

#: Which URL path params are required for this view.
progression_values = {
    "league": True,
    "week": False,
}


@progression_blueprint.url_defaults
def progression_url_defaults(endpoint: str, values: dict[str, Any]):
    """Wrapper for common defaults call. The ``values`` dict is mutated.

    :param endpoint: The flask endpoint.
    :param values: The flask values.
    """
    url_defaults(endpoint=endpoint, values=values, **progression_values)


@progression_blueprint.url_value_preprocessor
def progression_url_value_preprocessor(  # pylint: disable=unused-argument
    endpoint: str | None,
    values: dict[str, Any] | None,
):
    """Wrapper for common preprocessors call. Values are extracted then added to ``g``.

    :param endpoint: The flask endpoint.
    :param values: The flask values.
    """
    url_value_preprocessor(values=values, **progression_values)


@progression_blueprint.get("/season<season>/league<league>")
@login_required
@cache.cached(VIEW_CACHE_TTL)
def progression() -> str:
    """The route for a week-by-week progression graph of all scores for all users for a season.

    :return: The rendered template for the view.
    """
    sorted_season_results = results.view_progression(season=g.season, league_id=g.league)
    # Iterate over the sorted season totals and extract the information needed for scatter traces.
    if g.is_current_season_requested:
        if g.current_week is None:
            weeks = range(0)
        else:
            weeks = range(1, g.current_week + 1)
    else:
        weeks = range(1, g.weeks_in_season + 1)
    traces: list[dict[int | float, int | float]] = []
    names: list[str] = []
    visibilities: list[bool] = []
    for season_result in sorted_season_results:  # each is for a user
        traces.append(  # {week: finished_points}
            {
                week: season_result["point_rollups_by_week"].get(week, {"finished": 0})["finished"]
                for week in weeks
            }
        )
        names.append(g.id_name_map[season_result["user_id"]][:30])  # truncate so legend isn't huge
        visibilities.append(season_result["user_id"] == current_user.id)
    dumped_graph = form_scatter(
        traces=tuple(traces),
        title="Cumulative Points Over Weeks",
        names=tuple(names),
        visibilities=tuple(visibilities),
    )
    return render_template("contest/progression.html", dumped_graph=dumped_graph)
