"""The blueprint for the leaderboard views."""
from typing import Any
from flask import Blueprint, render_template, g
from flask_user import login_required
from supercontest.core import results
from supercontest.views.common import url_defaults, url_value_preprocessor, VIEW_CACHE_TTL
from supercontest.models import cache

#: The blueprint for the leaderboard views.
leaderboard_blueprint = Blueprint(
    "leaderboard",
    __name__,
    url_prefix="/leaderboard",
    template_folder="templates",
    static_folder="static",
)

#: Which URL path params are required for this view.
leaderboard_values = {
    "league": True,
    "week": False,
}


@leaderboard_blueprint.url_defaults
def leaderboard_url_defaults(endpoint: str, values: dict[str, Any]):
    """Wrapper for common defaults call. The ``values`` dict is mutated.

    :param endpoint: The flask endpoint.
    :param values: The flask values.
    """
    url_defaults(endpoint=endpoint, values=values, **leaderboard_values)


@leaderboard_blueprint.url_value_preprocessor
def leaderboard_url_value_preprocessor(  # pylint: disable=unused-argument
    endpoint: str | None,
    values: dict[str, Any] | None,
):
    """Wrapper for common preprocessors call. Values are extracted then added to ``g``.

    :param endpoint: The flask endpoint.
    :param values: The flask values.
    """
    url_value_preprocessor(values=values, **leaderboard_values)


@leaderboard_blueprint.get("/season<season>/league<league>")
@login_required
@cache.cached(VIEW_CACHE_TTL)
def leaderboard() -> str:
    """The route for all users' scores over the course of a season.

    :return: The rendered template for the view.
    """
    final_season_results, toprow_weeks, toprow_season, total_party = results.view_leaderboard(
        season=g.season,
        league_id=g.league,
    )
    return render_template(
        "contest/leaderboard.html",
        final_season_results=final_season_results,
        toprow_weeks=toprow_weeks,
        toprow_season=toprow_season,
        total_party=total_party,
    )
