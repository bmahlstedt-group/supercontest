"""The ``main`` blueprint is the collection of all other (non-contest) views, the ones that
don't require a season or week to be specified.
"""
import logging
from typing import Any
from flask import g, Blueprint, render_template, request, redirect, url_for, Response, current_app
from werkzeug.wrappers import Response as BaseResponse
from flask_user import current_user, login_required, roles_required
import stripe
from stripe.error import SignatureVerificationError
from supercontest.dbsession import queries, commits
from supercontest.core import picks, lines, results
from supercontest.forms import EmailPreferencesForm
from supercontest.util.csrf import csrf
from supercontest.models.schemas import GameSchema
from supercontest.models import cache, Game
from supercontest.views.common import VIEW_CACHE_TTL

logger = logging.getLogger(__name__)

#: The toplevel blueprint for everything that's not a contest view.
main_blueprint = Blueprint("main", __name__)


@main_blueprint.get("/")
@login_required
def home() -> BaseResponse:
    """The home route of the application.

    :return: A redirect to the desired (actual) landing page of the app.
    """
    if g.is_preseason or g.is_regseason:
        route = "matchups.matchups"
    else:
        route = "leaderboard.leaderboard"
    return redirect(url_for(route))


@main_blueprint.get("/feedback")
@login_required
def feedback() -> str:
    """A cheeky input form for user feedback.

    :return: The rendered template for the route.
    """
    return render_template("general/feedback.html")


@main_blueprint.get("/contact")
@login_required
def contact() -> str:
    """Contact form for user communication.

    :return: The rendered template for the route.
    """
    return render_template("general/contact.html")


@main_blueprint.route("/email_preferences", methods=["GET", "POST"])
@login_required
def email_preferences() -> str | BaseResponse:
    """Form in which users can change their notifications.

    :return: The rendered template for the route (if ``GET``) or a redirect home (if ``POST``).
    """
    form: Any = EmailPreferencesForm(obj=current_user)  # flask-wtf grabs request.form
    if form.validate_on_submit():  # makes sure it's a POST and calls validate
        form.populate_obj(current_user)
        current_app.user_manager.db_manager.save_object(current_user)  # pyright: ignore
        current_app.user_manager.db_manager.commit()  # pyright: ignore
        return redirect(url_for("main.home"))
    return render_template("flask_user/email_prefs.html", form=form)


@main_blueprint.post("/pick")
@login_required
def pick() -> Response:
    """This route handles picks from the client.

    :return: A flask response.
    """
    try:
        json: dict[Any, Any] = request.json if request.json is not None else {}
        season: int = json["season"]
        week: int = json["week"]
        teams: list[str] = json["picks"]
        picks.commit_picks(
            user_id=current_user.id,
            season=season,
            week=week,
            teams=teams,
        )
    except picks.InvalidPicks as exc:
        logger.exception("Invalid picks")
        return Response(status=400, response=str(exc))
    return Response(status=200)


@main_blueprint.post("/pick_late")
@roles_required("Admin")
def pick_late() -> BaseResponse:
    """Allows an admin to submit picks late for a user.

    :return: A redirect to the allpicks view.
    :raises ValueError: If not in a current week in the regular season.
    """
    user_email_lowered = request.form["user"]
    teams = [value for key, value in request.form.items() if "picked_team" in key]
    user_id = queries.get_user_from_email(email=user_email_lowered).id
    current_season, current_week = queries.get_current_season_and_week()
    if current_season is None or current_week is None:
        raise ValueError("Can only submit late picks from Sun -> Wed of the intended week")
    picks.commit_picks(
        user_id=user_id,
        season=current_season,
        week=current_week,
        teams=teams,
        emails=[user_email_lowered],
        verify=False,
    )
    return redirect(url_for("picks.picks"))


@main_blueprint.post("/commit_lines")
@roles_required("Admin")
def commit_lines() -> BaseResponse:
    """The route which handles the line commit every wednesday evening.

    :return: A redirect to the matchups view.
    """
    lines.commit_lines()
    return redirect(url_for("matchups.matchups"))


@main_blueprint.post("/change_status")
@roles_required("Admin")
def change_status() -> BaseResponse:
    """Allows an admin to change the status of a game. This is useful when
    the scorestrip is not realtime/live.

    :return: A redirect to the matchups view.
    """
    # The matchup on the form is shaped like "<id> <favorite> vs <underdog>"
    matchup = request.form["matchup"]
    game_id = int(matchup.split()[0])
    status_name = request.form["status_name"]
    commits.change_game_status(game_id=game_id, status_name=status_name)
    return redirect(url_for("matchups.matchups"))


@main_blueprint.get("/checkout")
@login_required
def checkout() -> str | BaseResponse:
    """Route handles stripe payment.

    :return: The rendered template for the checkout route with a loaded session, or a redirect home
        (if it's the offseason or the user is already in the paid league).
    """
    if current_app.config["DEBUG"]:
        stripe_public_api_key: Any = current_app.config["STRIPE_PUBLIC_KEY_API_TEST"]
    else:
        stripe_public_api_key: Any = current_app.config["STRIPE_PUBLIC_KEY_API"]
    if queries.is_offseason():
        logger.info("It is the offseason. Upgrading to a paid account is not allowed at this time.")
        return redirect(url_for("main.home"))
    current_season = queries.get_current_season()
    paid_league_id = queries.get_paid_league_id_for_season(season=current_season)
    if queries.is_user_in_league(user_id=current_user.id, league_id=paid_league_id):
        logger.info(
            f"User {current_user.email} tried to visit /checkout but is "
            + f"already in the paid league for {current_season} "
            + f"(id {paid_league_id}). Not creating a session and not "
            + "redirecting them to stripe."
        )
        return redirect(url_for("main.home"))
    stripe_checkout_session: Any = stripe.checkout.Session
    session: Any = stripe_checkout_session.create(
        success_url=request.url_root + url_for("main.checkout_success"),
        cancel_url=request.url_root,
        customer_email=current_user.email,
        client_reference_id=current_user.id,
        payment_method_types=["card"],
        mode="payment",
        line_items=[
            {
                "price_data": {
                    "currency": "usd",
                    "unit_amount": 5000,
                    "product_data": {
                        "name": f"Upgrade to Paid League for {current_season}",
                    },
                },
                "quantity": 1,
            }
        ],
    )
    return render_template(
        "checkout/checkout.html",
        public_api_key=stripe_public_api_key,
        session_id=session.id,
    )


@main_blueprint.get("/checkout_success")
@login_required
def checkout_success() -> str:
    """If a stripe checkout was successful.

    :return: The rendered template for the route.
    """
    return render_template("checkout/checkout_success.html")


@main_blueprint.post("/checkout_confirmation")
@csrf.exempt
def checkout_confirmation() -> Response:
    """When stripe sees a completed purchase, it sends a webhook event here. If it receives 200
    from this route, then it will forward the user to the success_url.

    :return: The flask response.
    """
    # Extract the proper secret based on environment.
    if current_app.config["DEBUG"]:
        stripe_webhook_secret: Any = current_app.config["STRIPE_SECRET_KEY_WEBHOOK_TEST"]
    else:
        stripe_webhook_secret: Any = current_app.config["STRIPE_SECRET_KEY_WEBHOOK"]
    # Analyze the event.
    try:
        stripe_sig = request.headers.get("Stripe-Signature")
        event: Any = stripe.Webhook.construct_event(
            request.data.decode("utf-8"),
            stripe_sig if stripe_sig is not None else "",
            stripe_webhook_secret,
        )
    except ValueError:
        logger.exception("Error while decoding stripe event in checkout_confirmation()")
        return Response(status=400, response="Bad payload")
    except SignatureVerificationError:
        return Response(status=400, response="Bad signature")
    # Actually add the user to the paid league in the db.
    if event["type"] == "checkout.session.completed":
        session: Any = event["data"]["object"]
        email: str = session["customer_email"]
        # user_id = int(session['client_reference_id'])
        logger.info(
            f"Received checkout_confirmation webhook (id: {event.id}) from stripe for {email}"
        )
        # season and league_id should be passed through stripe too, rather than re-queried,
        # because of the tiny chance someone tries to pay right at season crossover, but
        # we don't have to worry about that.
        commits.add_user_to_current_paid_league(user_email=email)
        logger.info("Successfully added them to the Paid League table.")
    return Response(status=200)


@main_blueprint.get("/alltimelb")
@login_required
@cache.cached(VIEW_CACHE_TTL)
def alltimelb() -> str:
    """Shows all league all user all season scores.

    :return: The rendered template for the route.
    """
    final_alltime_results, toprow_seasons, toprow_alltime = results.view_alltime_leaderboard()
    return render_template(
        "contest/alltimelb.html",
        final_alltime_results=final_alltime_results,
        toprow_seasons=toprow_seasons,
        toprow_alltime=toprow_alltime,
    )


@main_blueprint.get("/games/season<season>/week<week>")
@login_required
@cache.cached(VIEW_CACHE_TTL)
def get_games(season: str, week: str) -> list[Game]:
    """Fetched the sorted games, just like matchups/allpicks page load, but intended to be called
    async to update scores. This just runs the query. The frontend stitches the data into the view.

    :param season: Season year to fetch games for.
    :param week: Week within season to fetch games for.
    :return: List of sorted games (DB objects) for that week, serialized by that model's schema.
    """
    sorted_games = queries.get_ordered_games_in_week(season=int(season), week=int(week))
    game_schema: Any = GameSchema(many=True)
    dumped_sorted_games = game_schema.dump(sorted_games)
    return dumped_sorted_games
