"""The blueprint for the picks views."""
from typing import Any
from flask import Blueprint, render_template, g, jsonify, Response
from flask_user import login_required
from supercontest.core import results
from supercontest.models.schemas import GameSchema
from supercontest.views.common import url_defaults, url_value_preprocessor, VIEW_CACHE_TTL
from supercontest.models import cache

#: The blueprint for the picks views.
picks_blueprint = Blueprint("picks", __name__, url_prefix="/picks")

#: Which URL path params are required for this view.
picks_values = {
    "league": True,
    "week": True,
}


@picks_blueprint.url_defaults
def picks_url_defaults(endpoint: str, values: dict[str, Any]):
    """Wrapper for common defaults call. The ``values`` dict is mutated.

    :param endpoint: The flask endpoint.
    :param values: The flask values.
    """
    url_defaults(endpoint=endpoint, values=values, **picks_values)


@picks_blueprint.url_value_preprocessor
def picks_url_value_preprocessor(  # pylint: disable=unused-argument
    endpoint: str | None,
    values: dict[str, Any] | None,
):
    """Wrapper for common preprocessors call. Values are extracted then added to ``g``.

    :param endpoint: The flask endpoint.
    :param values: The flask values.
    """
    url_value_preprocessor(values=values, **picks_values)


@picks_blueprint.get("/season<season>/league<league>/week<week>")
@login_required
@cache.cached(VIEW_CACHE_TTL)
def picks() -> str:
    """The route to show ALL users' picks for a given week. Fails fast if picks aren't open.

    :return: The rendered template for the view.
    """
    if g.picks_open:
        return render_template("contest/picks.html")
    sorted_games, final_week_results, toprow_week_results = results.view_allpicks(
        season=g.season,
        week=g.week,
        league_id=g.league,
    )
    game_schema: Any = GameSchema(many=True)
    dumped_sorted_games = game_schema.dump(sorted_games)
    return render_template(
        "contest/picks.html",
        sorted_games=sorted_games,
        dumped_sorted_games=dumped_sorted_games,
        final_week_results=final_week_results,
        toprow_week_results=toprow_week_results,
    )


@picks_blueprint.get("/results/season<season>/league<league>/week<week>")
@login_required
@cache.cached(VIEW_CACHE_TTL)
def picks_results() -> Response:
    """The route to fetch the calculated week results for the allpicks view. Called async
    to just return the data (rather than template) in order to tick scores/results.

    :return: The list of results (rank, grade, picks, etc).
    """
    sorted_games, final_week_results, _ = results.view_allpicks(
        season=g.season,
        week=g.week,
        league_id=g.league,
    )
    game_schema: Any = GameSchema(many=True)
    dumped_sorted_games = game_schema.dump(sorted_games)
    return jsonify([dumped_sorted_games, final_week_results])
