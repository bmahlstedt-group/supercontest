"""The ``schedules`` blueprint is the collection of the routes that act as ingresses
for scheduled logic.
"""
import logging
from flask import Blueprint, Response
from supercontest.core.picks import email_all_picks, email_unfinished_pickers
from supercontest.core.scores import commit_scores
from supercontest.core.lines import commit_lines
from supercontest.util.csrf import csrf
from supercontest.views.common import service_account_only

logger = logging.getLogger(__name__)

#: The toplevel blueprint for endpoints that trigger scheduled tasks.
schedules_blueprint = Blueprint("schedules", __name__, url_prefix="/schedules")


@schedules_blueprint.post("/email-all-picks")
@csrf.exempt
@service_account_only
def email_all_picks_route() -> Response:
    """Entry point for scheduled behavior to email all picks after lockdown.

    :return: 200 if the email was sent. 304 if all is well, you don't need to
        send the email. 500 if there was an error in the attempt.
    """
    res = email_all_picks()
    return Response(status=200 if res is True else 304)


@schedules_blueprint.post("/email-unfinished-pickers")
@csrf.exempt
@service_account_only
def email_unfinished_pickers_route() -> Response:
    """Entry point for scheduled behavior to (before lockdown) reminder users who haven't picked.

    :return: 200 if the email was sent. 304 if all is well, you don't need to
        send the email. 500 if there was an error in the attempt.
    """
    res = email_unfinished_pickers()
    return Response(status=200 if res is True else 304)


@schedules_blueprint.post("/commit-lines")
@csrf.exempt
@service_account_only
def commit_lines_route() -> Response:
    """Entry point for scheduled behavior to fetch/commit lines when they open.

    :return: 200 if lines were committed. 304 if all is well, you don't need to
        commit lines. 500 if there was an error in the line fetch/commit attempt.
    """
    res = commit_lines()
    return Response(status=200 if res is True else 304)


@schedules_blueprint.post("/commit-scores")
@csrf.exempt
@service_account_only
def commit_scores_route() -> Response:
    """Entry point for scheduled behavior to check/commit live game scores.

    :return: 200 if scores were committed. 304 if all is well, you don't need to
        commit scores. 500 if there was an error in the score fetch/commit attempt.
    """
    res = commit_scores()
    return Response(status=200 if res is True else 304)
