"""The blueprint for the statistical views."""
from typing import Any
from flask import Blueprint, render_template, g
from flask_user import login_required
from supercontest.core import stats
from supercontest.views.common import url_defaults, url_value_preprocessor, VIEW_CACHE_TTL
from supercontest.models import cache

#: The blueprint for the statistical views.
statistics_blueprint = Blueprint("statistics", __name__, url_prefix="/statistics")

#: Which URL path params are required for this view.
statistics_values = {
    "league": False,
    "week": False,
    "stats_scope": True,
}


@statistics_blueprint.url_defaults
def statistics_url_defaults(endpoint: str, values: dict[str, Any]):
    """Wrapper for common defaults call. The ``values`` dict is mutated.

    :param endpoint: The flask endpoint.
    :param values: The flask values.
    """
    url_defaults(endpoint=endpoint, values=values, **statistics_values)


@statistics_blueprint.url_value_preprocessor
def statistics_url_value_preprocessor(  # pylint: disable=unused-argument
    endpoint: str | None,
    values: dict[str, Any] | None,
):
    """Wrapper for common preprocessors call. Values are extracted then added to ``g``.

    :param endpoint: The flask endpoint.
    :param values: The flask values.
    """
    url_value_preprocessor(values=values, **statistics_values)


@statistics_blueprint.get("/<stats_scope>/season<season>")
@login_required
@cache.cached(VIEW_CACHE_TTL)
def statistics() -> str:
    """Stats view. ``season=0`` means all seasons, and is cast here (``None`` means don't filter
    the subsequent query).

    :return: The rendered template for the route.
    """
    stats_builders = {
        "sbsc": stats.build_sbsc_plots,
        "nfl": stats.build_nfl_plots,
    }
    plots, numgames = stats_builders[g.stats_scope](season=g.season or None)
    return render_template("contest/stats.html", plots=plots, numgames=numgames)
