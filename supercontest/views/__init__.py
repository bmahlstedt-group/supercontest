# pyright: reportUnusedImport=false
"""The routes of the supercontest. This package defines and hoists the blueprints."""
from .main import main_blueprint
from .matchups import matchups_blueprint
from .picks import picks_blueprint
from .leaderboard import leaderboard_blueprint
from .progression import progression_blueprint
from .statistics import statistics_blueprint
from .schedules import schedules_blueprint
