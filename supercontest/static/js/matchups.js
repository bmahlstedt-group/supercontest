// Mutex for pick submission, client-server.
let picksClickable = true;

/**
 * Generic CSRF token handling for the AJAX requests.
 * (the token is added as metadata in layout.html)
 */
function setupCSRF() {
  const csrftoken = $('meta[name=csrf-token]').attr('content');
  $.ajaxSetup({
    beforeSend: function(xhr, settings) {
      if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
        xhr.setRequestHeader('X-CSRFToken', csrftoken);
      }
    },
  });
}

/**
 * Fetches new scores to tick w/o reload.
 * This intentionally does NOT have logic to skip if not in an
 * active week. It will fetch if the user is looking at an old season.
 * They likely won't leave an old tab open, but even if they do,
 * the frontend should try to sync with any changes to the backend.
 */
function updateGamesMatchups() {
  $.ajax({
    type: 'GET',
    url: '/games/season' + season + '/week' + week,
    success: function(data) {
      sortedGames = data; // update global
      renderMatchupRows();
    },
    error: function(request, status, message) {
      // error for serverside. the same problem is "warn" clientside.
      $.notify(request.responseText, 'error');
    },
  });
}

/**
 * Iterates through the returned list of Game objects (dumped by our schema)
 * to render the rows of the Matchups table.
 */
function renderMatchupRows() {
  const table = document.getElementById('matchupsTable');
  table.innerHTML = ''; // wipe it before adding
  sortedGames.forEach((game) => {
    const row = table.insertRow();
    row.classList.add('matchup');
    const favorite = getFavorite(game.contestants);
    const underdog = getUnderdog(game.contestants);

    let cell = row.insertCell();
    const d = new Date(game['datetime']);
    cell.textContent = d.toLocaleString('en-US', { timeZone: 'America/Los_Angeles', weekday: 'short', hour: 'numeric', minute: 'numeric', hour12: true });

    cell = row.insertCell();
    let gameLocation = 'Neutral';
    if (favorite.location.name == 'Home') {
      gameLocation = favorite.team.city;
    } else if (underdog.location.name == 'Home') {
      gameLocation = underdog.team.city;
    }
    cell.textContent = gameLocation;
    cell.classList.add('border-right-thick');

    cell = row.insertCell();
    cell.textContent = '-' + game.line;

    cell = row.insertCell();
    cell.style.backgroundImage = getLogoUrl(favorite.team.name);
    cell.classList.add('logo');
    cell.classList.add('px-4');

    cell = row.insertCell();
    cell.textContent = favorite.team.name;
    cell.classList.add('favoredTeam');
    colorPickCell(cell, favorite.coverage.name, 'Cover', 'Push', game.status, null, pickTeams.includes(favorite.team.name));

    cell = row.insertCell();
    cell.textContent = favorite.score;
    cell.classList.add('px-2');

    cell = row.insertCell();
    cell.textContent = underdog.score;
    cell.classList.add('px-2');

    cell = row.insertCell();
    cell.textContent = underdog.team.name;
    cell.classList.add('underdogTeam');
    colorPickCell(cell, underdog.coverage.name, 'Cover', 'Push', game.status, null, pickTeams.includes(underdog.team.name));

    cell = row.insertCell();
    cell.style.backgroundImage = getLogoUrl(underdog.team.name);
    cell.classList.add('logo');
    cell.classList.add('px-4');

    cell = row.insertCell();
    cell.textContent = '+' + game.line;

    cell = row.insertCell();
    cell.textContent = game.status.name;
    cell.classList.add('border-left-thick');
  });
}

/**
 * Change cursor to pointer if hoverable over pickable cell.
 */
function registerHover() {
  $('tr.matchup > td.favoredTeam, td.underdogTeam').hover(function() {
    if (picksOpen && picksClickable) {
      $(this).css('cursor', 'pointer');
    }
  });
}

/**
 * Pick handling as team cells are clicked.
 */
function registerClick() {
  $('tr.matchup > td.favoredTeam, td.underdogTeam').click(function() {
    // Initialize the matchup data for analysis.
    const team = $(this).text();
    const opponentColumn = $(this).hasClass('favoredTeam') ? 'underdogTeam' : 'favoredTeam';
    const opponent = $(this).closest('tr').find('td.' + opponentColumn).text();
    // Grab the corresponding matchup info.
    const game = sortedGames.find(game => ((game.contestants[0].team.name == team) || (game.contestants[1].team.name == team)));
    // Verify picks are open.
    if (!picksOpen) {
      $.notify('Picks can only be placed Wednesday-Saturday (Pacific Time) during a season', 'warn');
      return;
    }
    // Check the mutex for one-pick-at-a-time.
    if (!picksClickable) {
      $.notify('A pick is already being submitted, hang tight', 'warn');
      return;
    }
    // Fail fast if the game isn't in the future.
    if (!game.status.unstarted) {
      $.notify('The ' + team + ' game has already started', 'warn');
      return;
    }
    // Main pick switch. Either deselect an existing pick, swap a pick for the
    // opponent in the same matchup, or select a new pick.
    if (pickTeams.includes(team)) {
      pickTeams.splice(pickTeams.indexOf(team), 1);
      submitPicks($(this).get(0));
    } else if (pickTeams.includes(opponent)) {
      pickTeams.splice(pickTeams.indexOf(opponent), 1);
      pickTeams.push(team);
      submitPicks($(this).get(0));
    } else {
      if (pickTeams.length >= 5) {
        $.notify('You cannot select more than 5 teams per week', 'warn');
        return;
      }
      pickTeams.push(team);
      submitPicks($(this).get(0));
    }
  });
}

/** Submission of picks via AJAX. This just sends the pickTeams list,
 * so it is used to both add and remove picks.
 * @param {object} el the element
 */
function submitPicks(el) {
  picksClickable = false;
  wipePickColorUnstarted(el);
  el.classList.add('pick-unsubmitted');
  $.ajax({
    type: 'POST',
    url: '/pick',
    contentType: 'application/json; charset=UTF-8',
    data: JSON.stringify({
      'picks': pickTeams,
      'week': week,
      'season': season,
    }),
    success: function(data) {
      $.notify('Pick(s) submitted successfully', 'success');
      el.classList.remove('pick-unsubmitted');
      renderMatchupRows();
      registerHover();
      registerClick();
      picksClickable = true;
    },
    error: function(request, status, message) {
      // error for serverside. the same problem is "warn" clientside.
      $.notify(request.responseText, 'error');
    },
  });
}

/**
 * Functionality for the "pick random 5" button. Checks if there are
 * pending picks being submitted. Filters out started games as
 * unpickable. "Are picks open" logic is handled (clientside) in the
 * presence of the button (serverside checks too, remember).
 */
function registerPick5Random() {
  $('#pick5Rrandom').click(function() {
    if (!picksClickable) {
      $.notify('A pick is already being submitted, hang tight', 'warn');
      return;
    }
    const matchups = sortedGames.filter(
        (x) => x.status.unstarted
    ).map(
        (x) => [x.contestants[0].team.name, x.contestants[1].team.name]
    );
    // Wipe the existing picks. We have to detect if any of the previously
    // picked teams have started games. Those are locked. So we don't
    // randomly select 5, we select the remaining number of picks after locks.
    const startedTeams = sortedGames.filter(
        (x) => !x.status.unstarted
    ).map(
        (x) => [x.contestants[0].team.name, x.contestants[1].team.name]
    ).flat();
    const keepTeams = pickTeams.filter((x) => startedTeams.includes(x));
    pickTeams.length = 0;
    keepTeams.forEach((x) => pickTeams.push(x));
    for (let i = 0; i < (5-keepTeams.length); i++) {
      if (!matchups.length) {
        break;
      }
      const matchupIndex = Math.floor( Math.random()*matchups.length );
      const matchup = matchups[matchupIndex];
      matchups.splice(matchupIndex, 1);
      const teamIndex = Math.floor( Math.random()*matchup.length );
      const team = matchup[teamIndex];
      pickTeams.push(team);
    }
    submitPicks($(this).get(0));
  });
}

if (window.location.href.indexOf('matchups') != -1) {
  setupCSRF();
  if (sortedGames.length > 0) {
    renderMatchupRows();
    registerHover();
    registerClick();
    registerPick5Random();
    setInterval(updateGamesMatchups, 60 * 1000);
  }
}
