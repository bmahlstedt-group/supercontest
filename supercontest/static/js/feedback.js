/**
 * Overwrites the text that the user types into the feedback box.
 */
function feedbackReplacer() {
  $('#feedbackTextBox').keydown(function(event) {
    const forceStr = 'petty\'s mom ';
    const forceStrLen = forceStr.length;
    event.preventDefault();
    event.stopPropagation();
    const currentStrLen = $(this).val().length;
    const repetitions = Math.floor(currentStrLen / forceStrLen);
    const newSlot = currentStrLen % forceStrLen;
    $(this).val(forceStr.repeat(repetitions) + forceStr.substr(0, newSlot + 1));
  });
}

if (window.location.href.indexOf('feedback') != -1) {
  feedbackReplacer();
}
