/**
 * Loop over every score for every user for every week in the leaderboard,
 * coloring and adding tooltips and such. This is just for the central part
 * of the table, the columns per week.
 */
function styleLeaderboardRows() {
  $('tr.userRow').each(function(rank) {
    const pickCountsByWeek = finalSeasonResults[rank]['pick_counts_by_week'];
    const pointRollupsByWeek = finalSeasonResults[rank]['point_rollups_by_week'];
    $(this).find('td.lbWeekScore').each(function(weekFrom0) {
      const week = weekFrom0 + 1; // pick counts dict is indexed from 1 for week
      if (typeof pickCountsByWeek[week] === 'undefined') {
        $(this).text('-');
        $(this).addClass('text-body-secondary');
        return true; // if no user data for that week, continue to next week
      }
      const pickCounts = pickCountsByWeek[week];
      const possiblePoints = pickCounts['cover'] +  pickCounts['push'] +  pickCounts['noncover'];
      const numPicks = sumValues(pickCounts);
      const totalPoints = typeof pointRollupsByWeek[week] === 'undefined' ? 0 : pointRollupsByWeek[week]['finished'];
      let cellText = '-'; // default
      if (!isCurrentSeasonRequested || currentWeek == 0) { // old season or offseason
        cellText = totalPoints.toString();
      } else {
        if (week == currentWeek) { // current week in current season
          cellText = totalPoints.toString() + '/' + possiblePoints.toString();
        } else if (week < currentWeek) { // past week in current season
          cellText = totalPoints.toString();
        }
      }
      $(this).attr('title', 'submitted ' + numPicks.toString() + ' pick(s)');
      if (totalPoints == 5 && possiblePoints == 5) {
        $(this).addClass('pick-finished-cover');
      } else if (totalPoints == 0 && possiblePoints == 5) {
        $(this).addClass('pick-finished-noncover');
      }
      // Make the cell a link to the picks view (matchups for
      // current_user, allpicks for everyone else).
      const userId = $(this).parent().get(0).id;
      let route = '';
      if (userId == currentUserId) {
        route = 'https://southbaysupercontest.com/matchups/season' + season +'/week' + week;
      } else {
        route = 'https://southbaysupercontest.com/picks/season' + season + '/league' + league +'/week' + week;
      }
      $(this).html('<a style="text-decoration: none;" href=' + route +'>' + cellText + '</a>');
    });
  });
}

if (window.location.href.indexOf('leaderboard') != -1) {
  checkInLeague();
  styleLeaderboardRows();
  colorRanks();
  addRankSuffixes();
  highlightUserRow();
  colorPercentages();
}
