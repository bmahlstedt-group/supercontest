/**
 * Serverside plotly (python) defines the layouts and json-encodes the figures.
 */
function renderPlots() {
  const plotsToc = document.getElementById('plots-toc');
  const plotsCard = document.getElementById('plots-card');
  plots.forEach(function(plot, i) {
    // Add the div for the plot.
    const div = document.createElement('div');
    const id = 'plot' + i.toString();
    div.id = id;
    div.classList.add('ms-5');
    plotsCard.appendChild(div);
    // Plot.
    Plotly.plot(id, JSON.parse(plot));
    // Add the hr before the next.
    const hr = document.createElement('hr');
    hr.classList.add('mt-0');
    plotsCard.appendChild(hr);
    // Add a link to this anchor at the top ToC.
    const listItem = document.createElement('li');
    const link = document.createElement('a');
    link.href = '#' + id;
    plotObj = JSON.parse(plot);
    link.innerText = plotObj.layout.title.text;
    listItem.appendChild(link);
    plotsToc.appendChild(listItem);
  });
  // Remove the last hr.
  plotsCard.removeChild(plotsCard.lastChild);
}

if (window.location.href.indexOf('statistics') != -1) {
  renderPlots();
}
