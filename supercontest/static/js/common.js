/**
 * Removes classes that color the pick cell.
 * @param {object} el the cell to wipe color from
 */
function wipePickColorUnstarted(el) {
  el.classList.remove('pick-unstarted');
  el.classList.remove('unpick-unstarted');
  el.classList.remove('pick-unsubmitted');
}

/**
 * Colors a pick cell with inprogress vs finished, cover vs noncover vs push.
 * @param {object} teamEl the element to color
 * @param {string} value the result (TEAMNAME or PUSH)
 * @param {string} successCriteria the team name
 * @param {string} tieCriteria usually PUSH
 * @param {string} gameStatus as defined serverside (DB obj from statuses table)
 * @param {object} opponentEl the element of the opposing team
 * @param {boolean} picked whether they picked this team
 */
function colorPickCell(
    teamEl,
    value,
    successCriteria,
    tieCriteria,
    gameStatus,
    opponentEl = null,
    picked = true) {
  const prefixPicked = picked ? 'pick-' : 'unpick-';
  if (gameStatus.unstarted) {
    wipePickColorUnstarted(teamEl);
    teamEl.classList.add(prefixPicked + 'unstarted');
    if (opponentEl) {
      wipePickColorUnstarted(opponentEl);
      opponentEl.classList.add(prefixPicked + 'unstarted');
    }
    return;
  }
  const prefixStatus = gameStatus.finished ? 'finished-' : 'inprogress-';
  if (value === successCriteria) {
    teamEl.classList.add(prefixPicked + prefixStatus + 'cover');
    if (opponentEl) {
      opponentEl.classList.add(prefixPicked + prefixStatus + 'noncover');
    }
  } else if (value === tieCriteria) {
    teamEl.classList.add(prefixPicked + prefixStatus + 'push');
    if (opponentEl) {
      opponentEl.classList.add(prefixPicked + prefixStatus + 'push');
    }
  } else {
    teamEl.classList.add(prefixPicked + prefixStatus + 'noncover');
    if (opponentEl) {
      opponentEl.classList.add(prefixPicked + prefixStatus + 'cover');
    }
  }
}

/**
 * Defines colors for 1st/2nd/3rd.
 * @param {object} el the element to check
 */
function colorRankCell(el) {
  switch (parseInt(el.textContent)) {
    case 1:
      el.classList.add('rank-first');
      break;
    case 2:
      el.classList.add('rank-second');
      break;
    case 3:
      el.classList.add('rank-third');
      break;
  }
}

/**
 * Colors the cell based on ranking, 1st or 2nd or 3rd or nothing.
 * Requires that the rows have class="userRow" and child cells have
 * class="rankCell"
 */
function colorRanks() {
  $('tr.userRow').each(function() {
    const rankEl = $(this).find('.rankCell').get(0);
    colorRankCell(rankEl);
  });
}

/**
 * Defines colors for above 50 / below 50 / equal 50.
 * @param {object} el the element to check
 */
function colorPercentage(el) {
  const val = parseFloat(el.textContent);
  switch (true) {
    case val > 50.0:
      el.classList.add('perc-above-50');
      break;
    case val < 50.0:
      el.classList.add('perc-below-50');
      break;
    case val == 50.0:
      el.classList.add('perc-equal-50');
      break;
    default:
      console.warn('percentage cell does not have range above/below/equal 50.');
  }
}

/**
 * Colors the cell based on percentage, above or below 50.
 * Requires the rows have class="userRow" and the child has class="percentage"
 */
function colorPercentages() {
  $('tr.userRow').each(function() {
    const percEl = $(this).find('.percentage').get(0);
    colorPercentage(percEl);
  });
}

/**
 * Takes a number and returns the ranking (eg "st" for "1st").
 * @param {number} i the number to get the suffix for
 * @return {string}
 */
function ordinalSuffixOf(i) {
  const j = i % 10;
  const k = i % 100;
  if (j == 1 && k != 11) {
    return 'st';
  }
  if (j == 2 && k != 12) {
    return 'nd';
  }
  if (j == 3 && k != 13) {
    return 'rd';
  }
  return 'th';
}

/**
 * Iterates through cells and adds the rank suffixes.
 * Requires the cells have class="rankCell" and the child has class="rankSuffix"
 */
function addRankSuffixes() {
  $('.rankCell').each(function() {
    const text = $(this).text();
    const suffixTextEl = $(this).find('.rankSuffix');
    const suffixText = ordinalSuffixOf(text);
    suffixTextEl.text(suffixText);
  });
}

/**
 * Goes through the user rows and highlights that of the current logged-in user.
 * Requires that the rows have class="userRow" as well as id="<userId>"
 */
function highlightUserRow() {
  $('tr.userRow').each(function() {
    const userRowId = parseInt($(this).attr('id'));
    if (currentUserId == userRowId) {
      $(this).get(0).classList.add('table-info');
    }
  });
}

/**
 * Registers flashed messages. Right now, the only portion of the
 * app that sends flashes is flask-user. The frontend uses notify.js.
 * The flask-user categories are success/error, which match notify.js.
 */
function flashMessages() {
  if (typeof flashedMessages !== 'undefined' && flashedMessages.length != 0) {
    $.each(flashedMessages, function(i, data) {
      const category = data[0];
      const message = data[1];
      // The user knows to sign in from the redirect. Don't show them an error.
      if (!message.includes('You must be signed in to access')) {
        $.notify(message, {className: category, autoHideDelay: 5000});
      }
    });
  }
}

/**
 * Flash a message to the user if they're on a view for a league
 * that they're not in.
 */
function checkInLeague() {
  if (typeof userInLeague !== 'undefined') {
    if (!userInLeague) {
      $.notify('You are not in this league', 'info');
    }
  }
}

// Do these on all views. The js loads in the bundle for everything.
flashMessages();

/**
 * Takes a length 2 list of contestants in a matchup and returns the favorite.
 * If the game is pickem, will return the first. Matches the python function.
 * @param {array} contestants List of contestants
 * @return {string} Favorite contestant
 */
function getFavorite(contestants) {
  for (const contestant of contestants) {
    if (contestant.prediction.name == 'Favorite') {
      return contestant;
    }
  }
  return contestants[0];
}

/**
 * Takes a length 2 list of contestants in a matchup and returns the underdog.
 * If the game is pickem, will return the second. Matches the python function.
 * @param {array} contestants List of contestants
 * @return {string} Underdog contestant
 */
function getUnderdog(contestants) {
  for (const contestant of contestants) {
    if (contestant.prediction.name == 'Underdog') {
      return contestant;
    }
  }
  return contestants[1];
}

/**
 * Takes a team name and returns the url for its logo.
 * @param {string} team Name of the team, case insensitive
 * @return {string} URL of that team's logo
 */
function getLogoUrl(team) {
  return 'url(https://southbaysupercontest.com/assets/teams/' + team.toUpperCase() + '.svg)';
}

/**
 * Calculates the sum of an iterable.
 * @param {string} obj Object containing the values to sum
 * @return {float} The sum
 */
function sumValues(obj) {
  return Object.values(obj).reduce((a, b) => a + b, 0);
}
