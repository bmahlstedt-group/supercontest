from typing import Any

UserMixin: Any
login_required: Any
roles_required: Any
UserManager: Any
current_user: Any

# flask_user also attaches the user_manager object to flask.current_app, but I'll fix that
# later when I deprecate the yanked flask_user. I don't want to stub flask, it comes with
# good typing already.
