from supercontest.core.picks import commit_picks
from supercontest.dbsession import queries

USER_EMAIL = "brian.mahlstedt@gmail.com"
SEASON = 2023
WEEK = 3
TEAMS = ["Saints", "Patriots", "Steelers", "Colts", "Rams"]

user = queries.get_user_from_email(email=USER_EMAIL)
commit_picks(user_id=user.id, season=SEASON, week=WEEK, teams=TEAMS, verify=False)