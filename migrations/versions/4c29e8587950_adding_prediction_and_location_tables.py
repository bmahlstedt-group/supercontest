"""adding prediction and location tables

Revision ID: 4c29e8587950
Revises: 91b87c06388a
Create Date: 2023-03-14 18:08:53.461767

"""
from typing import Any
from alembic import op
import sqlalchemy as sa

op: Any

# revision identifiers, used by Alembic.
revision = '4c29e8587950'
down_revision = '91b87c06388a'
branch_labels = None
depends_on = None


def upgrade():
    # Change coverage table to plural table name and capitalized name col values.
    op.rename_table('coverage', 'coverages')
    op.execute("UPDATE coverages SET name = 'Noncover' WHERE name = 'noncover'")
    op.execute("UPDATE coverages SET name = 'Push' WHERE name = 'push'")
    op.execute("UPDATE coverages SET name = 'Cover' WHERE name = 'cover'")

    # Create new locations table.
    locations_table = op.create_table(
        'locations',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    locations_rows = [
        {"name": "Visitor"},
        {"name": "Neutral"},
        {"name": "Home"},
    ]
    op.bulk_insert(table=locations_table, rows=locations_rows)

    # Create new predictions table.
    predictions_table = op.create_table(
        'predictions',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    predictions_rows = [
        {"name": "Underdog"},
        {"name": "Pickem"},
        {"name": "Favorite"},
    ]
    op.bulk_insert(table=predictions_table, rows=predictions_rows)

    # Go through lines and change favorite/underdog/home to team1/2 with prediction/location.
    with op.batch_alter_table('lines', schema=None) as batch_op:
        batch_op.add_column(sa.Column('team1_id', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('team1_prediction_id', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('team1_location_id', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('team2_id', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('team2_prediction_id', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('team2_location_id', sa.Integer(), nullable=True))
        batch_op.create_foreign_key(None, 'teams', ['team2_id'], ['id'])
        batch_op.create_foreign_key(None, 'locations', ['team2_location_id'], ['id'])
        batch_op.create_foreign_key(None, 'locations', ['team1_location_id'], ['id'])
        batch_op.create_foreign_key(None, 'predictions', ['team2_prediction_id'], ['id'])
        batch_op.create_foreign_key(None, 'predictions', ['team1_prediction_id'], ['id'])
        batch_op.create_foreign_key(None, 'teams', ['team1_id'], ['id'])
    op.execute('UPDATE lines SET team1_id = favored_team_id')
    op.execute(
        'UPDATE lines SET team1_prediction_id = CASE '
        '  WHEN line = 0 THEN 2 '
        '  ELSE 3 '
        'END'
    )
    op.execute(
        'UPDATE lines SET team1_location_id = CASE '
        '  WHEN home_team_id IS NULL THEN 2 '
        '  WHEN home_team_id = favored_team_id THEN 3 '
        '  ELSE 1 '
        'END'
    )
    op.execute('UPDATE lines SET team2_id = underdog_team_id')
    op.execute(
        'UPDATE lines SET team2_prediction_id = CASE '
        '  WHEN line = 0 THEN 2 '
        '  ELSE 1 '
        'END'
    )
    op.execute(
        'UPDATE lines SET team2_location_id = CASE '
        '  WHEN home_team_id IS NULL THEN 2 '
        '  WHEN home_team_id = underdog_team_id THEN 3 '
        '  ELSE 1 '
        'END'
    )
    with op.batch_alter_table('lines', schema=None) as batch_op:
        batch_op.alter_column('team1_id', nullable=False)
        batch_op.alter_column('team1_prediction_id', nullable=False)
        batch_op.alter_column('team1_location_id', nullable=False)
        batch_op.alter_column('team2_id', nullable=False)
        batch_op.alter_column('team2_prediction_id', nullable=False)
        batch_op.alter_column('team2_location_id', nullable=False)
        batch_op.drop_constraint('lines_favored_team_id_fkey', type_='foreignkey')
        batch_op.drop_constraint('lines_underdog_team_id_fkey', type_='foreignkey')
        batch_op.drop_constraint('lines_home_team_id_fkey', type_='foreignkey')
        batch_op.drop_column('favored_team_id')
        batch_op.drop_column('underdog_team_id')
        batch_op.drop_column('home_team_id')

    # Change scores to set coverage individually for team1/2.
    with op.batch_alter_table('scores', schema=None) as batch_op:
        batch_op.add_column(sa.Column('team1_score', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('team2_score', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('team1_coverage_id', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('team2_coverage_id', sa.Integer(), nullable=True))
        batch_op.create_foreign_key(None, 'coverages', ['team1_coverage_id'], ['id'])
        batch_op.create_foreign_key(None, 'coverages', ['team2_coverage_id'], ['id'])
    op.execute('UPDATE scores SET team1_score = favored_team_score')
    op.execute('UPDATE scores SET team2_score = underdog_team_score')
    op.execute(
        'UPDATE scores SET team1_coverage_id = subquery.coverage_id '
        'FROM (SELECT '
        '          scores2.id AS scores_id, '
        '          CASE '
        '              WHEN scores2.coverer_id IS NULL THEN 2 '
        '              WHEN scores2.coverer_id = lines.team1_id THEN 3'
        '              ELSE 1'
        '          END AS coverage_id '
        '      FROM scores AS scores2 '
        '      JOIN lines ON scores2.line_id = lines.id '
        '     ) AS subquery '
        'WHERE scores.id = subquery.scores_id'
    )
    op.execute(
        'UPDATE scores SET team2_coverage_id = subquery.coverage_id '
        'FROM (SELECT '
        '          scores2.id AS scores_id, '
        '          CASE '
        '              WHEN scores2.coverer_id IS NULL THEN 2 '
        '              WHEN scores2.coverer_id = lines.team2_id THEN 3'
        '              ELSE 1'
        '          END AS coverage_id '
        '      FROM scores AS scores2 '
        '      JOIN lines ON scores2.line_id = lines.id '
        '     ) AS subquery '
        'WHERE scores.id = subquery.scores_id'
    )
    with op.batch_alter_table('scores', schema=None) as batch_op:
        batch_op.alter_column('team1_score', nullable=False)
        batch_op.alter_column('team2_score', nullable=False)
        batch_op.alter_column('team1_coverage_id', nullable=False)
        batch_op.alter_column('team2_coverage_id', nullable=False)
        batch_op.drop_constraint('scores_coverer_id_fkey', type_='foreignkey')
        batch_op.drop_column('coverer_id')
        batch_op.drop_column('underdog_team_score')
        batch_op.drop_column('favored_team_score')

    # Drop coverage from picks. It's now transitive via pick.line.score.*
    with op.batch_alter_table('picks', schema=None) as batch_op:
        batch_op.drop_constraint('picks_coverage_id_fkey', type_='foreignkey')
        batch_op.drop_column('coverage_id')


def downgrade():
    pass
