"""wiping partial data from 2019 week 11

Revision ID: cca63b25a176
Revises: 856719006dc3
Create Date: 2023-01-16 18:26:36.121478

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cca63b25a176'
down_revision = '856719006dc3'
branch_labels = None
depends_on = None


def upgrade():
    op.execute('DELETE FROM picks WHERE line_id >= 712 AND line_id <= 725;')
    op.execute('DELETE FROM scores WHERE line_id >= 712 AND line_id <= 725;')
    op.execute('DELETE FROM lines where week_id=28')


def downgrade():
    pass
