"""adding 2014-17 users

Revision ID: ef26ff495867
Revises: 7158b9592402
Create Date: 2023-01-18 08:43:58.898286

"""
from alembic import op
import sqlalchemy as sa

from datetime import datetime
from supercontest.models import db, User


# revision identifiers, used by Alembic.
revision = 'ef26ff495867'
down_revision = '7158b9592402'
branch_labels = None
depends_on = None


def upgrade():
    users = [
        User(first_name='Aaron', last_name='Dues', email='aarondues@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Andi', last_name='Spector', email='andixspector@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Andy', last_name='OConnor', email='ajoconnor514@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Brad', last_name='Atwood', email='bradatwood22@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Brandon', last_name='Latulippe', email='blatulip24@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='JJ', last_name='Joseph', email='Jj@fantasyiq.co', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='John', last_name='Monaco', email='todthill@pacbell.net', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Les', last_name='Kim', email='leskim_1@hotmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Matt', last_name='Lavoie', email='mlavoie1981@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Michael', last_name='Lopez', email='lolay07@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Michael', last_name='Lovejoy', email='michaeljlovejoy@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Michael', last_name='Moss', email='mikemoss88@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Peter', last_name='Puglese', email='peter.puglese@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Ryan', last_name='McGee', email='RyanMcGee726@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Stephen', last_name='Phillips', email='homer.s.phillips@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Steve', last_name='Krackhardt', email='stephen.krackhardt@spacex.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Trevor', last_name='Harris', email='trevordanielharris@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
        User(first_name='Will', last_name='Bruey', email='wpb35@cornell.edu', email_confirmed_at=datetime.utcnow(), active=True, password='og', email_when_picks_open=False, email_when_picks_closing=False, email_all_picks=False),
    ]
    db.session.add_all(users)
    db.session.commit()

def downgrade():
    pass
