"""adding paid leagues per season

Revision ID: 6c52ea2689d6
Revises: 1540990ac02b
Create Date: 2023-01-19 10:09:58.424714

"""
from alembic import op
import sqlalchemy as sa

from supercontest.dbsession import queries
from supercontest.models import db, League
from supercontest.models.models import league_user


# revision identifiers, used by Alembic.
revision = '6c52ea2689d6'
down_revision = '1540990ac02b'
branch_labels = None
depends_on = None


def upgrade():

    # Init
    seasons = [
        2014,
        2015,
        2016,
        2017,
        2018,
        2019,
        2020,
        2021,
        2022,
    ]
    free_leaguers = {  # these are NOT added to paid league_user_association
        2014: [],
        2015: [],
        2016: [],
        2017: [],
        2018: [
            'timothyqle@gmail.com',
            'caseymoser01@gmail.com',
        ],
        2019: [
            'jon.pawelk@gmail.com',
            'allisoncrutchfield1@gmail.com',
            'whatacan@yahoo.com',
            'nsjames88@gmail.com',
            'rsluyter2@yahoo.com',
            'tburdette87@yahoo.com',
            'sink.wood37@gmail.com',
            'lynne.defilippo@gmail.com',
            'avgarcia@gmail.com',
            'bryancchan@gmail.com',
        ],
        2020: [
            'artavetisyan24@gmail.com',
            'whatacan@yahoo.com',
            'andrewwhite175@gmail.com',
            'lynne.defilippo@gmail.com',
        ],
        2021: [
            'artavetisyan24@gmail.com',
            'nsjames88@gmail.com',
            'sam.e.jones21@gmail.com',
            'cchorro@gmail.com',
        ],
        2022: [
            'fcastaneda650@gmail.com',
            'dcontreras15@gmail.com',
            'william.st.hilaire@gmail.com',
            'sjkerch@gmail.com',
        ],
    }

    # Wipe the current leagues and league-user associations (all rows).
    db.session.execute(sa.delete(league_user))
    db.session.execute(sa.delete(League.__table__))
    db.session.commit()

    # Add leagues.
    leagues = [League(name='Paid', season_id=queries.get_season_id(season=season))
               for season in seasons]
    db.session.add_all(leagues)
    db.session.commit()

    # Then associate users with the leagues (you don't insert directly into the
    # association table with sqlalchemy, you update the direct tables that have
    # the relationship and the ORM will populate the association table).
    for season in seasons:
        all_pickers = queries.get_users_in_season(season=season, return_obj=True)
        paid_league = queries.get_paid_league_for_season(season=season, return_obj=True)
        for user in all_pickers:
            if user.email not in free_leaguers[season]:
                paid_league.users.append(user)  # the association/relationship
    db.session.commit()


def downgrade():
    pass
