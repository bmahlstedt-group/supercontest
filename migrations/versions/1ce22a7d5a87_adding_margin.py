"""adding margin

Revision ID: 1ce22a7d5a87
Revises: 129c511a81f3
Create Date: 2023-03-17 16:34:37.099581

"""
from typing import Any
from alembic import op
import sqlalchemy as sa

op: Any

# revision identifiers, used by Alembic.
revision = '1ce22a7d5a87'
down_revision = '129c511a81f3'
branch_labels = None
depends_on = None


def upgrade():
    # Add the margin column to the opponents table.
    op.add_column('opponents', sa.Column('margin', sa.Float(), nullable=True))


def downgrade():
    # Drop the col.
    op.drop_column('opponents', 'margin')
