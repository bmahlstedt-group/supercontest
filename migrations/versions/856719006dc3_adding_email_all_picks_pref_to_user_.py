"""adding email_all_picks pref to user table

Revision ID: 856719006dc3
Revises: e4db69b069c9
Create Date: 2022-11-04 15:27:32.683490

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '856719006dc3'
down_revision = 'e4db69b069c9'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('users', sa.Column('email_all_picks', sa.Boolean()))
    op.execute("UPDATE users SET email_all_picks = email_when_picks_open")
    op.alter_column('users', 'email_all_picks', nullable=False)


def downgrade():
    with op.batch_alter_table('users', schema=None) as batch_op:
        batch_op.drop_column('email_all_picks')
