"""adding lines picks scores 2023 week 2

Revision ID: c383cb62ad76
Revises: 4db2dbac6dc8
Create Date: 2023-09-21 01:56:32.995384

"""
from supercontest.core import picks, lines
from supercontest.dbsession import queries, commits
from supercontest.util.types import ScoresFetch

# revision identifiers, used by Alembic.
revision = 'c383cb62ad76'
down_revision = '4db2dbac6dc8'
branch_labels = None
depends_on = None


def upgrade():
    # this adds the lines and then the picks and then the scores

    # general
    season = 2023
    week = 2

    # lines
    fetched_lines = lines.fetch_lines(week=week)
    week_id = queries.get_week_id(season=season, week=week)
    commits.insert_contestants(week_id=week_id, linesfetch=fetched_lines)

    # picks
    picks.commit_picks(user_id=114, season=season, week=week, teams=['PACKERS', 'BEARS', 'CARDINALS', 'PATRIOTS', 'SAINTS'], emails=None, verify=False)
    picks.commit_picks(user_id=13, season=season, week=week, teams=['CHARGERS', 'TEXANS', 'LIONS', 'CHIEFS', 'RAMS'], emails=None, verify=False)
    picks.commit_picks(user_id=12, season=season, week=week, teams=['LIONS', 'CHIEFS', 'JETS', 'COMMANDERS', 'BROWNS'], emails=None, verify=False)
    picks.commit_picks(user_id=6, season=season, week=week, teams=['CHARGERS', 'COLTS', 'RAIDERS', 'RAMS', 'JETS'], emails=None, verify=False)
    picks.commit_picks(user_id=22, season=season, week=week, teams=['LIONS', 'BEARS', 'CARDINALS', 'DOLPHINS', 'BROWNS'], emails=None, verify=False)
    picks.commit_picks(user_id=23, season=season, week=week, teams=['LIONS', 'JAGUARS', '49ERS', 'COWBOYS', 'DOLPHINS'], emails=None, verify=False)
    picks.commit_picks(user_id=20, season=season, week=week, teams=['FALCONS', 'TEXANS', 'BEARS', 'BENGALS', 'CARDINALS'], emails=None, verify=False)
    picks.commit_picks(user_id=1, season=season, week=week, teams=['TITANS', 'PACKERS', 'BEARS', 'JAGUARS', 'DOLPHINS'], emails=None, verify=False)
    picks.commit_picks(user_id=5, season=season, week=week, teams=['PACKERS', 'TEXANS', 'RAIDERS', '49ERS', 'SAINTS'], emails=None, verify=False)
    picks.commit_picks(user_id=17, season=season, week=week, teams=['LIONS', 'BEARS', 'BILLS', 'CARDINALS', 'BROWNS'], emails=None, verify=False)
    picks.commit_picks(user_id=10, season=season, week=week, teams=['TITANS', 'PACKERS', 'BUCCANEERS', 'COMMANDERS', 'STEELERS'], emails=None, verify=False)
    picks.commit_picks(user_id=39, season=season, week=week, teams=['PACKERS', 'CHIEFS', 'RAVENS', 'CARDINALS', 'STEELERS'], emails=None, verify=False)
    picks.commit_picks(user_id=33, season=season, week=week, teams=['PACKERS', 'RAIDERS', 'RAVENS', 'DOLPHINS', 'SAINTS'], emails=None, verify=False)
    picks.commit_picks(user_id=4, season=season, week=week, teams=['LIONS', 'CHIEFS', 'CARDINALS', 'JETS', 'DOLPHINS'], emails=None, verify=False)
    picks.commit_picks(user_id=117, season=season, week=week, teams=['CHIEFS', 'BENGALS', 'COWBOYS', 'STEELERS', ''], emails=None, verify=False)
    picks.commit_picks(user_id=2, season=season, week=week, teams=['CHIEFS', 'RAVENS', '49ERS', 'DOLPHINS', 'SAINTS'], emails=None, verify=False)
    picks.commit_picks(user_id=91, season=season, week=week, teams=['TITANS', 'LIONS', 'BILLS', 'BENGALS', 'COWBOYS'], emails=None, verify=False)
    picks.commit_picks(user_id=31, season=season, week=week, teams=['PACKERS', 'SEAHAWKS', 'BEARS', 'CHIEFS', 'COWBOYS'], emails=None, verify=False)
    picks.commit_picks(user_id=72, season=season, week=week, teams=['FALCONS', 'CHIEFS', '49ERS', 'JETS', 'SAINTS'], emails=None, verify=False)
    picks.commit_picks(user_id=71, season=season, week=week, teams=['CHARGERS', 'PACKERS', 'TEXANS', 'CARDINALS', 'BROWNS'], emails=None, verify=False)
    picks.commit_picks(user_id=25, season=season, week=week, teams=['TITANS', 'LIONS', 'BUCCANEERS', 'BILLS', 'JETS'], emails=None, verify=False)
    picks.commit_picks(user_id=3, season=season, week=week, teams=['BEARS', 'RAIDERS', 'JAGUARS', '49ERS', 'SAINTS'], emails=None, verify=False)
    picks.commit_picks(user_id=86, season=season, week=week, teams=['CHARGERS', 'FALCONS', 'COLTS', 'LIONS', 'BEARS'], emails=None, verify=False)
    picks.commit_picks(user_id=14, season=season, week=week, teams=['COLTS', 'LIONS', 'RAIDERS', 'CHIEFS', 'RAVENS'], emails=None, verify=False)
    picks.commit_picks(user_id=58, season=season, week=week, teams=['CHIEFS', 'RAVENS', '49ERS', 'COWBOYS', 'BRONCOS'], emails=None, verify=False)
    picks.commit_picks(user_id=78, season=season, week=week, teams=['TITANS', 'BEARS', 'BENGALS', '49ERS', 'DOLPHINS'], emails=None, verify=False)
    picks.commit_picks(user_id=40, season=season, week=week, teams=['FALCONS', 'BUCCANEERS', 'BILLS', 'BENGALS', 'BROWNS'], emails=None, verify=False)
    picks.commit_picks(user_id=15, season=season, week=week, teams=['TITANS', 'COLTS', 'RAVENS', 'COMMANDERS', 'SAINTS'], emails=None, verify=False)

    # scores
    sbsc_games = queries.get_games(season, week)
    games = [
        next(_game for _game in sbsc_games if "Vikings" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Ravens" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Seahawks" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Colts" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Bears" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Chiefs" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Packers" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Raiders" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Chargers" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "49ers" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Giants" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Jets" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Commanders" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Dolphins" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Saints" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Browns" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),

    ]
    scores = [
        ScoresFetch(team_scores={"Vikings": 28, "Eagles": 34}, status="FINAL"),
        ScoresFetch(team_scores={"Ravens": 27, "Bengals": 24}, status="FINAL"),
        ScoresFetch(team_scores={"Seahawks": 37, "Lions": 31}, status="OT"),
        ScoresFetch(team_scores={"Colts": 31, "Texans": 20}, status="FINAL"),
        ScoresFetch(team_scores={"Bears": 17, "Buccaneers": 27}, status="FINAL"),
        ScoresFetch(team_scores={"Chiefs": 17, "Jaguars": 9}, status="FINAL"),
        ScoresFetch(team_scores={"Packers": 24, "Falcons": 25}, status="FINAL"),
        ScoresFetch(team_scores={"Raiders": 10, "Bills": 38}, status="FINAL"),
        ScoresFetch(team_scores={"Chargers": 24, "Titans": 27}, status="OT"),
        ScoresFetch(team_scores={"49ers": 30, "Rams": 23}, status="FINAL"),
        ScoresFetch(team_scores={"Giants": 31, "Cardinals": 28}, status="FINAL"),
        ScoresFetch(team_scores={"Jets": 10, "Cowboys": 30}, status="FINAL"),
        ScoresFetch(team_scores={"Commanders": 35, "Broncos": 33}, status="FINAL"),
        ScoresFetch(team_scores={"Dolphins": 24, "Patriots": 17}, status="FINAL"),
        ScoresFetch(team_scores={"Saints": 20, "Panthers": 17}, status="FINAL"),
        ScoresFetch(team_scores={"Browns": 22, "Steelers": 26}, status="FINAL"),
    ]
    commits.write_scores(games=games, scores=scores)


def downgrade():
    pass
