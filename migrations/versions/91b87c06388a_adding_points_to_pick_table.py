"""adding points to pick table

Revision ID: 91b87c06388a
Revises: 978e373c9572
Create Date: 2023-03-04 21:59:07.132120

"""
from typing import Any
from alembic import op
import sqlalchemy as sa

op: Any

# revision identifiers, used by Alembic.
revision = '91b87c06388a'
down_revision = '978e373c9572'
branch_labels = None
depends_on = None


def upgrade():
    # Add new column.
    op.add_column("picks", sa.Column('coverage_id', sa.Integer(), nullable=True))
    op.create_foreign_key(
        constraint_name=None,
        source_table="picks",
        local_cols=["coverage_id"],
        referent_table="coverage",
        remote_cols=["id"], 
    )
    # Populate it.
    stmt = (
        "UPDATE picks "
        "SET coverage_id = subquery.coverage_id "
        "FROM (SELECT "
        "          picks2.id AS picks_id, "
        "          CASE "
        "              WHEN scores.coverer_id IS NULL "
        "                  THEN 2 "
        "              WHEN pick_teams.name = coverers.name "
        "                  THEN 3 "
        "              ELSE "
        "                  1 "
        "          END AS coverage_id"
        "      FROM picks AS picks2 "
        "          JOIN lines ON picks2.line_id = lines.id "
        "          JOIN scores ON lines.id = scores.line_id "
        "          LEFT JOIN teams AS coverers ON scores.coverer_id = coverers.id "
        "          JOIN teams AS pick_teams ON picks2.team_id = pick_teams.id "
        "      ) AS subquery "
        "WHERE picks.id = subquery.picks_id"
    )
    op.execute(stmt)
    # Make it not-nullable.
    op.alter_column(table_name="picks", column_name="coverage_id", nullable=False)


def downgrade():
    pass
