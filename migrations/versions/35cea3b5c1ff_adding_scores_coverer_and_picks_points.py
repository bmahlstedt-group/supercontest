"""adding scores.coverer and picks.points

Revision ID: 35cea3b5c1ff
Revises: bf7750e02308
Create Date: 2019-10-01 19:04:36.167174

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '35cea3b5c1ff'
down_revision = 'bf7750e02308'
branch_labels = None
depends_on = None


def upgrade():
    # This is just an old change to keep up with.
    with op.batch_alter_table('leagues', schema=None) as batch_op:
        batch_op.alter_column('name',
               existing_type=sa.VARCHAR(),
               nullable=False)

    # Add the column, fill it in with a comparison for all old data, then make it not nullable.
    op.add_column('scores', sa.Column('coverer', sa.String()))
    op.execute('UPDATE scores '
               'SET coverer = CASE '
               '    WHEN (favored_team_score - underdog_team_score) > line THEN favored_team '
               '    WHEN (favored_team_score - underdog_team_score) < line THEN underdog_team '
               '    ELSE \'PUSH\' '
               'END '
               'FROM lines '
               'WHERE line_id = lines.id')
    op.alter_column('scores', 'coverer', nullable=False)

    # Add the column, fill it in with a comparison for all old data, then make it not nullable.
    op.add_column('picks', sa.Column('points', sa.Float()))
    op.execute('UPDATE picks '
               'SET points = CASE '
               '    WHEN team = coverer THEN 1.0 '
               '    WHEN coverer = \'PUSH\' THEN 0.5 '
               '    ELSE 0.0 '
               'END '
               # No cols from the lines table are used, it just connects picks-scores.
               'FROM scores, lines '
               'WHERE scores.line_id = lines.id '
               'AND picks.line_id = lines.id')
    op.alter_column('picks', 'points', nullable=False)


def downgrade():
    with op.batch_alter_table('scores', schema=None) as batch_op:
        batch_op.drop_column('coverer')

    with op.batch_alter_table('picks', schema=None) as batch_op:
        batch_op.drop_column('points')

    with op.batch_alter_table('leagues', schema=None) as batch_op:
        batch_op.alter_column('name',
               existing_type=sa.VARCHAR(),
               nullable=True)
