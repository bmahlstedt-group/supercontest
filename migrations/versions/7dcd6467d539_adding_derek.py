"""adding derek

Revision ID: 7dcd6467d539
Revises: fc9ac51dc037
Create Date: 2023-09-14 00:46:47.549516

"""
from sqlalchemy import text
from datetime import datetime
from supercontest.models import db, User


# revision identifiers, used by Alembic.
revision = '7dcd6467d539'
down_revision = 'fc9ac51dc037'
branch_labels = None
depends_on = None


def upgrade():
    # Add the new users who submitted the gform week1 but don't have an sbsc account yet.
    db.session.add_all([User(first_name='Derek', last_name='Perez', email='derek.perez@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='derek', email_when_picks_open=True, email_when_picks_closing=True, email_all_picks=True)])
    db.session.add_all([User(first_name='Ben', last_name='Freiborg', email='Benfreiborg@icloud.com', email_confirmed_at=datetime.utcnow(), active=True, password='ben', email_when_picks_open=True, email_when_picks_closing=True, email_all_picks=True)])
    db.session.add_all([User(first_name='Brad', last_name='LBW', email='bradlbw@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='derek', email_when_picks_open=True, email_when_picks_closing=True, email_all_picks=True)])
    db.session.add_all([User(first_name='Rich', last_name='Harner', email='rharner1969@gmail.com', email_confirmed_at=datetime.utcnow(), active=True, password='derek', email_when_picks_open=True, email_when_picks_closing=True, email_all_picks=True)])
    db.session.commit()
    # Disable jacyln's notifications.
    db.session.execute(text("UPDATE users SET email_all_picks = false where id=74"))
    db.session.execute(text("UPDATE users SET email_when_picks_open = false where id=74"))
    db.session.execute(text("UPDATE users SET email_when_picks_closing = false where id=74"))

def downgrade():
    
    pass
