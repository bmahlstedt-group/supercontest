"""adding picks 2023 week 1

Revision ID: 4db2dbac6dc8
Revises: 7dcd6467d539
Create Date: 2023-09-14 01:21:17.267780

"""
from supercontest.core import picks, lines
from supercontest.dbsession import queries, commits
from supercontest.util.types import ScoresFetch

# revision identifiers, used by Alembic.
revision = '4db2dbac6dc8'
down_revision = '7dcd6467d539'
branch_labels = None
depends_on = None


def upgrade():
    # this adds the lines and then the picks and then the scores

    # general
    season = 2023
    week = 1

    # lines
    fetched_lines = lines.fetch_lines(week=week)
    week_id = queries.get_week_id(season=season, week=week)
    commits.insert_contestants(week_id=week_id, linesfetch=fetched_lines)

    # picks
    picks.commit_picks(user_id=86, season=season, week=week, teams=['PANTHERS', 'RAVENS', 'BENGALS', 'JAGUARS', 'VIKINGS'], emails=None, verify=False)
    picks.commit_picks(user_id=91, season=season, week=week, teams=['BENGALS', 'JAGUARS', 'VIKINGS', 'BEARS', 'BILLS'], emails=None, verify=False)
    picks.commit_picks(user_id=33, season=season, week=week, teams=['PANTHERS', 'BENGALS', 'RAIDERS', 'EAGLES', 'BILLS'], emails=None, verify=False)
    picks.commit_picks(user_id=12, season=season, week=week, teams=['BROWNS', 'JAGUARS', '49ERS', 'COMMANDERS', 'SEAHAWKS'], emails=None, verify=False)
    picks.commit_picks(user_id=77, season=season, week=week, teams=['BENGALS', 'COLTS', 'STEELERS', 'CARDINALS', 'RAMS'], emails=None, verify=False)
    picks.commit_picks(user_id=32, season=season, week=week, teams=['BENGALS', 'VIKINGS', 'CHARGERS', 'SEAHAWKS', 'BILLS'], emails=None, verify=False)
    picks.commit_picks(user_id=23, season=season, week=week, teams=['PANTHERS', 'COMMANDERS', 'RAIDERS', 'RAMS', 'GIANTS'], emails=None, verify=False)
    picks.commit_picks(user_id=14, season=season, week=week, teams=['JAGUARS', 'VIKINGS', '49ERS', 'BEARS', 'EAGLES'], emails=None, verify=False)
    picks.commit_picks(user_id=20, season=season, week=week, teams=['FALCONS', 'BUCCANEERS', 'TITANS', 'DOLPHINS', 'EAGLES'], emails=None, verify=False)
    picks.commit_picks(user_id=25, season=season, week=week, teams=['BENGALS', 'BRONCOS', 'EAGLES', 'BILLS', ''], emails=None, verify=False)
    picks.commit_picks(user_id=10, season=season, week=week, teams=['STEELERS', 'CARDINALS', 'DOLPHINS', 'EAGLES', 'GIANTS'], emails=None, verify=False)
    picks.commit_picks(user_id=3, season=season, week=week, teams=['RAVENS', 'TITANS', 'PATRIOTS', 'GIANTS', 'JETS'], emails=None, verify=False)
    picks.commit_picks(user_id=5, season=season, week=week, teams=['FALCONS', 'BROWNS', 'TITANS', 'STEELERS', 'PACKERS'], emails=None, verify=False)
    picks.commit_picks(user_id=1, season=season, week=week, teams=['JAGUARS', 'BEARS', 'DOLPHINS', 'GIANTS', 'BILLS'], emails=None, verify=False)
    picks.commit_picks(user_id=72, season=season, week=week, teams=['RAVENS', 'BUCCANEERS', 'EAGLES', 'SEAHAWKS', 'COWBOYS'], emails=None, verify=False)
    picks.commit_picks(user_id=71, season=season, week=week, teams=['49ERS', 'COMMANDERS', 'BEARS', 'EAGLES', 'COWBOYS'], emails=None, verify=False)
    picks.commit_picks(user_id=58, season=season, week=week, teams=['BEARS', 'BRONCOS', 'DOLPHINS', 'COWBOYS', 'BILLS'], emails=None, verify=False)
    picks.commit_picks(user_id=6, season=season, week=week, teams=['JAGUARS', 'VIKINGS', 'BRONCOS', 'DOLPHINS', 'JETS'], emails=None, verify=False)
    picks.commit_picks(user_id=114, season=season, week=week, teams=['RAVENS', 'BENGALS', 'TITANS', 'CARDINALS', 'EAGLES'], emails=None, verify=False)
    picks.commit_picks(user_id=2, season=season, week=week, teams=['FALCONS', 'BROWNS', 'BEARS', 'DOLPHINS', 'EAGLES'], emails=None, verify=False)
    picks.commit_picks(user_id=17, season=season, week=week, teams=['JAGUARS', 'VIKINGS', 'STEELERS', 'BEARS', 'EAGLES'], emails=None, verify=False)
    picks.commit_picks(user_id=78, season=season, week=week, teams=['FALCONS', 'JAGUARS', 'VIKINGS', 'TITANS', 'EAGLES'], emails=None, verify=False)
    picks.commit_picks(user_id=15, season=season, week=week, teams=['COLTS', '49ERS', 'EAGLES', 'COWBOYS', 'JETS'], emails=None, verify=False)
    picks.commit_picks(user_id=40, season=season, week=week, teams=['TITANS', 'COMMANDERS', 'RAIDERS', 'EAGLES', 'SEAHAWKS'], emails=None, verify=False)
    picks.commit_picks(user_id=4, season=season, week=week, teams=['VIKINGS', 'COMMANDERS', 'BEARS', 'CHARGERS', 'SEAHAWKS'], emails=None, verify=False)
    picks.commit_picks(user_id=15, season=season, week=week, teams=['TITANS', '49ERS', 'COMMANDERS', 'EAGLES', 'COWBOYS'], emails=None, verify=False)
    picks.commit_picks(user_id=39, season=season, week=week, teams=['BENGALS', 'VIKINGS', '49ERS', 'EAGLES', 'BILLS'], emails=None, verify=False)
    picks.commit_picks(user_id=115, season=season, week=week, teams=['FALCONS', 'TEXANS', 'BENGALS', 'JAGUARS', 'VIKINGS'], emails=None, verify=False)
    picks.commit_picks(user_id=31, season=season, week=week, teams=['BENGALS', 'VIKINGS', 'DOLPHINS', 'PATRIOTS', 'SEAHAWKS'], emails=None, verify=False)
    picks.commit_picks(user_id=22, season=season, week=week, teams=['RAVENS', 'SAINTS', '49ERS', 'PACKERS', 'BRONCOS'], emails=None, verify=False)
    picks.commit_picks(user_id=116, season=season, week=week, teams=['PACKERS', 'BRONCOS', 'CHARGERS', 'EAGLES', 'SEAHAWKS'], emails=None, verify=False)
    picks.commit_picks(user_id=117, season=season, week=week, teams=['PACKERS', 'RAIDERS', 'EAGLES', 'RAMS', 'GIANTS'], emails=None, verify=False)

    # scores
    sbsc_games = queries.get_games(season, week)
    games = [
        next(_game for _game in sbsc_games if "Lions" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Bengals" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Texans" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Buccaneers" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Panthers" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Cardinals" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Jaguars" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "49ers" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Titans" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Raiders" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Eagles" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Rams" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Dolphins" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Packers" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Cowboys" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),
        next(_game for _game in sbsc_games if "Bills" in [_game.contestants[0].team.name, _game.contestants[1].team.name]),

    ]
    scores = [
        ScoresFetch(team_scores={"Lions": 21, "Chiefs": 20}, status="FINAL"),
        ScoresFetch(team_scores={"Bengals": 3, "Browns": 24}, status="FINAL"),
        ScoresFetch(team_scores={"Texans": 9, "Ravens": 25}, status="FINAL"),
        ScoresFetch(team_scores={"Buccaneers": 20, "Vikings": 17}, status="FINAL"),
        ScoresFetch(team_scores={"Panthers": 10, "Falcons": 24}, status="FINAL"),
        ScoresFetch(team_scores={"Cardinals": 16, "Commanders": 20}, status="FINAL"),
        ScoresFetch(team_scores={"Jaguars": 31, "Colts": 21}, status="FINAL"),
        ScoresFetch(team_scores={"49ers": 30, "Steelers": 7}, status="FINAL"),
        ScoresFetch(team_scores={"Titans": 15, "Saints": 16}, status="FINAL"),
        ScoresFetch(team_scores={"Raiders": 17, "Broncos": 16}, status="FINAL"),
        ScoresFetch(team_scores={"Eagles": 25, "Patriots": 20}, status="FINAL"),
        ScoresFetch(team_scores={"Rams": 30, "Seahawks": 13}, status="FINAL"),
        ScoresFetch(team_scores={"Dolphins": 36, "Chargers": 34}, status="FINAL"),
        ScoresFetch(team_scores={"Packers": 38, "Bears": 20}, status="FINAL"),
        ScoresFetch(team_scores={"Cowboys": 40, "Giants": 0}, status="FINAL"),
        ScoresFetch(team_scores={"Bills": 16, "Jets": 22}, status="OT"),

    ]
    commits.write_scores(games=games, scores=scores)


def downgrade():
    pass
