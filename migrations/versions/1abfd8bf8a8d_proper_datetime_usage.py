"""proper datetime usage

Revision ID: 1abfd8bf8a8d
Revises: f781368d2af7
Create Date: 2019-09-18 14:52:14.359595

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1abfd8bf8a8d'
down_revision = 'f781368d2af7'
branch_labels = None
depends_on = None


def upgrade():
    # Convert all existing Line.datetime from str to actual datetime.
    op.alter_column('lines', 'datetime', type_=sa.DateTime(timezone=True),
                    postgresql_using='datetime::timestamp with time zone')
    # Convert all the existing 2018 Line.datetime values to UTC. This assumes 
    # the migration will be run before DST End in 2019 (Sun Nov 3 2am PDT).
    # Games in the winter were PST, 8 hours shifted.
    op.execute('UPDATE lines '
               'SET datetime = datetime + INTERVAL \'8 hours\' '
               'WHERE lines.datetime >  TIMESTAMP \'2018-11-04 02:00:00\' '
               'AND lines.datetime < TIMESTAMP \'2019-03-10 02:00:00\'')
    # Games in the fall 2018 and fall 2019 were PDT, 7 hours shifted.
    op.execute('UPDATE lines '
               'SET datetime = datetime + INTERVAL \'7 hours\' '
               'WHERE lines.datetime <  TIMESTAMP \'2018-11-04 02:00:00\' '
               'OR lines.datetime > TIMESTAMP \'2019-03-10 02:00:00\'')

    # Add the season start and end date cols.
    op.add_column('seasons', sa.Column('season_start', sa.Date()))
    op.add_column('seasons', sa.Column('season_end', sa.Date()))
    # Update the existing rows with the season starts/ends.
    for season in [2018, 2019]:
        op.execute('UPDATE seasons '
                   'SET season_start = \'{}-09-01\', season_end = \'{}-02-01\' '
                   'WHERE seasons.season = {}'.format(season, season+1, season))
    # Now that they're populated, make them not nullable.
    op.alter_column('seasons', 'season_start', nullable=False)
    op.alter_column('seasons', 'season_end', nullable=False)

    # Add the week start and end date cols.
    op.add_column('weeks', sa.Column('week_start', sa.DateTime(timezone=True)))
    op.add_column('weeks', sa.Column('week_end', sa.DateTime(timezone=True)))
    # Explicitly fill out the rest of the week inserts for the 2019 season (which
    # is season_id 2) starting hardcoded at week 3.
    new_weeks = range(3, 17+1)
    new_weeks_values = ', '.join(['(2, {})'.format(week) for week in new_weeks])
    op.execute('INSERT INTO weeks (season_id, week) '
               'VALUES {}'.format(new_weeks_values))

    # Now update the rows with the datetimes for the week starts/ends.
    initial_conds = {1: '2018-09-05', 2: '2019-09-04'}
    for season_id, week_1_start in initial_conds.items():
        op.execute('UPDATE weeks '
                   'SET week_start = TIMESTAMP \'{} 17:00:00\' + '  # season opener always pdt
                                    'INTERVAL \'7 hours\' + '  # convert to utc
                                    'INTERVAL \'1 week\' * (weeks2.rn - 1) '
                                    'FROM (SELECT id, row_number() OVER (ORDER BY week) rn '
                                          'FROM weeks '
                                          'WHERE season_id = {} '
                                          ') weeks2 '
                                    'WHERE weeks2.id = weeks.id'.format(week_1_start, season_id))
    op.execute('UPDATE weeks '
               'SET week_end = week_start + INTERVAL \'1 week\'')
    # Now that they're populated, make them not nullable.
    op.alter_column('weeks', 'week_start', nullable=False)
    op.alter_column('weeks', 'week_end', nullable=False)


def downgrade():
    # This downgrade would be easy to drop the *_start/end cols, but
    # converting the proper datetimes back to ambiguous strings would never
    # be done deliberately. Just restore if need be.
    raise Exception('Irreversible migration. Major version change. If '
                    'you need to go back, check the vcs hash date of this '
                    'version file and then revert to a backup that was '
                    'dumped with a timestamp before then.')
