"""adding league table

Revision ID: bf7750e02308
Revises: 1abfd8bf8a8d
Create Date: 2019-09-23 14:27:06.297382

"""
from alembic import op
import sqlalchemy as sa

from sqlalchemy.schema import Sequence, CreateSequence

# revision identifiers, used by Alembic.
revision = 'bf7750e02308'
down_revision = '1abfd8bf8a8d'
branch_labels = None
depends_on = None


def upgrade():
    # Create both new tables.
    op.execute(CreateSequence(Sequence('leagues_id_seq')))
    op.create_table('leagues',
                    sa.Column('id', sa.Integer(),
                    server_default=sa.text("nextval('leagues_id_seq')"),
                    nullable=False),
        sa.Column('name', sa.String()),
        sa.Column('season_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['season_id'], ['seasons.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table('league_user_association',
    sa.Column('league_id', sa.Integer(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['league_id'], ['leagues.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], )
    )

def downgrade():
    op.drop_table('league_user_association')
    op.drop_table('leagues')
