"""adding coverer col to scores table via observer

Revision ID: d7d1ff396e12
Revises: c0fc20041238
Create Date: 2023-03-04 02:59:10.925507

"""
from typing import Any
from alembic import op
import sqlalchemy as sa

op: Any

# revision identifiers, used by Alembic.
revision = 'd7d1ff396e12'
down_revision = 'c0fc20041238'
branch_labels = None
depends_on = None


def upgrade():
    # Add new column.
    op.add_column("scores", sa.Column('coverer_id', sa.Integer(), nullable=True))
    op.create_foreign_key(
        constraint_name=None,
        source_table="scores",
        local_cols=["coverer_id"],
        referent_table="teams",
        remote_cols=["id"], 
    )
    # Populate it.
    stmt = (
        "UPDATE scores "
        "SET coverer_id = subquery.coverer_id "
        "FROM (SELECT "
        "          scores2.id AS scores_id, "
        "          CASE "
        "              WHEN scores2.favored_team_score - scores2.underdog_team_score > lines.line "
        "                  THEN favored_teams.id "
        "              WHEN scores2.favored_team_score - scores2.underdog_team_score = lines.line "
        "                  THEN NULL "
        "              ELSE "
        "                  underdog_teams.id "
        "          END AS coverer_id"
        "      FROM scores AS scores2 "
        "          JOIN lines ON scores2.line_id = lines.id "
        "          JOIN teams AS favored_teams ON lines.favored_team_id = favored_teams.id "
        "          JOIN teams AS underdog_teams ON lines.underdog_team_id = underdog_teams.id "
        "      ) AS subquery "
        "WHERE scores.id = subquery.scores_id"
    )
    op.execute(stmt)
    # Note that we leave `Score.coverer` nullable, for the case where the teams are currently
    # pushing and there's no coverer.


def downgrade():
    pass
