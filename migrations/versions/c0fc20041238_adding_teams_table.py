"""adding teams table

Revision ID: c0fc20041238
Revises: 96c8d56cebb7
Create Date: 2023-03-02 02:07:05.464516

"""
from typing import Any
from alembic import op
import sqlalchemy as sa

op: Any

# revision identifiers, used by Alembic.
revision = 'c0fc20041238'
down_revision = '96c8d56cebb7'
branch_labels = None
depends_on = None


def upgrade():
    # Create the new table.
    teams_table = op.create_table(
        'teams',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('abbv', sa.String(), nullable=False),
        sa.Column('city', sa.String(), nullable=False),
        sa.Column('espn_city', sa.String(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    # Insert the static data into the new table.
    teams_rows = [
        {'name': '49ers', 'abbv': 'SF', 'city': 'San Francisco', 'espn_city': 'San Francisco'},
        {'name': 'Bears', 'abbv': 'CHI', 'city': 'Chicago', 'espn_city': 'Chicago'},
        {'name': 'Bengals', 'abbv': 'CIN', 'city': 'Cincinnati', 'espn_city': 'Cincinnati'},
        {'name': 'Bills', 'abbv': 'BUF', 'city': 'Buffalo', 'espn_city': 'Buffalo'},
        {'name': 'Broncos', 'abbv': 'DEN', 'city': 'Denver', 'espn_city': 'Denver'},
        {'name': 'Browns', 'abbv': 'CLE', 'city': 'Cleveland', 'espn_city': 'Cleveland'},
        {'name': 'Buccaneers', 'abbv': 'TB', 'city': 'Tampa Bay', 'espn_city': 'Tampa Bay'},
        {'name': 'Cardinals', 'abbv': 'ARI', 'city': 'Arizona', 'espn_city': 'Arizona'},
        {'name': 'Chargers', 'abbv': 'LAC', 'city': 'Los Angeles', 'espn_city': 'Los Angeles'},
        {'name': 'Chiefs', 'abbv': 'KC', 'city': 'Kansas City', 'espn_city': 'Kansas City'},
        {'name': 'Colts', 'abbv': 'IND', 'city': 'Indianapolis', 'espn_city': 'Indianapolis'},
        {'name': 'Commanders', 'abbv': 'WAS', 'city': 'Washington', 'espn_city': 'Washington'},
        {'name': 'Cowboys', 'abbv': 'DAL', 'city': 'Dallas', 'espn_city': 'Dallas'},
        {'name': 'Dolphins', 'abbv': 'MIA', 'city': 'Miami', 'espn_city': 'Miami'},
        {'name': 'Eagles', 'abbv': 'PHI', 'city': 'Philadelphia', 'espn_city': 'Philadelphia'},
        {'name': 'Falcons', 'abbv': 'ATL', 'city': 'Atlanta', 'espn_city': 'Atlanta'},
        {'name': 'Giants', 'abbv': 'NYG', 'city': 'New York', 'espn_city': 'NY Giants'},
        {'name': 'Jaguars', 'abbv': 'JAX', 'city': 'Jacksonville', 'espn_city': 'Jacksonville'},
        {'name': 'Jets', 'abbv': 'NYJ', 'city': 'New York', 'espn_city': 'NY Jets'},
        {'name': 'Lions', 'abbv': 'DET', 'city': 'Detroit', 'espn_city': 'Detroit'},
        {'name': 'Packers', 'abbv': 'GB', 'city': 'Green Bay', 'espn_city': 'Green Bay'},
        {'name': 'Panthers', 'abbv': 'CAR', 'city': 'Carolina', 'espn_city': 'Carolina'},
        {'name': 'Patriots', 'abbv': 'NE', 'city': 'New England', 'espn_city': 'New England'},
        {'name': 'Raiders', 'abbv': 'LV', 'city': 'Las Vegas', 'espn_city': 'Las Vegas'},
        {'name': 'Rams', 'abbv': 'LAR', 'city': 'Los Angeles', 'espn_city': 'Los Angeles'},
        {'name': 'Ravens', 'abbv': 'BAL', 'city': 'Baltimore', 'espn_city': 'Baltimore'},
        {'name': 'Saints', 'abbv': 'NO', 'city': 'New Orleans', 'espn_city': 'New Orleans'},
        {'name': 'Seahawks', 'abbv': 'SEA', 'city': 'Seattle', 'espn_city': 'Seattle'},
        {'name': 'Steelers', 'abbv': 'PIT', 'city': 'Pittsburgh', 'espn_city': 'Pittsburgh'},
        {'name': 'Texans', 'abbv': 'HOU', 'city': 'Houston', 'espn_city': 'Houston'},
        {'name': 'Titans', 'abbv': 'TEN', 'city': 'Tennessee', 'espn_city': 'Tennessee'},
        {'name': 'Vikings', 'abbv': 'MIN', 'city': 'Minnesota','espn_city': 'Minnesota'},
    ]
    op.bulk_insert(table=teams_table, rows=teams_rows)
    # Create the new cols and FKs. Make the column nullable to start (we need to populate it still).
    op.add_column(table_name="lines", column=sa.Column('favored_team_id', sa.Integer(), nullable=True))
    op.add_column(table_name="lines", column=sa.Column('underdog_team_id', sa.Integer(), nullable=True))
    op.add_column(table_name="lines", column=sa.Column('home_team_id', sa.Integer(), nullable=True))
    op.add_column(table_name="picks", column=sa.Column('team_id', sa.Integer(), nullable=True))
    op.create_foreign_key(constraint_name=None, source_table="lines", local_cols=["favored_team_id"], referent_table="teams", remote_cols=["id"])
    op.create_foreign_key(constraint_name=None, source_table="lines", local_cols=["underdog_team_id"], referent_table="teams", remote_cols=["id"])
    op.create_foreign_key(constraint_name=None, source_table="lines", local_cols=["home_team_id"], referent_table="teams", remote_cols=["id"])
    op.create_foreign_key(constraint_name=None, source_table="picks", local_cols=["team_id"], referent_table="teams", remote_cols=["id"])
    # Populate the col by doing a lookup of the current col value against the name in the new table.
    op.execute("UPDATE lines SET favored_team_id = teams.id FROM teams WHERE UPPER(teams.name) = lines.favored_team")
    op.execute("UPDATE lines SET underdog_team_id = teams.id FROM teams WHERE UPPER(teams.name) = lines.underdog_team")
    op.execute("UPDATE lines SET home_team_id = teams.id FROM teams WHERE UPPER(teams.name) = lines.home_team")
    op.execute("UPDATE picks SET team_id = teams.id FROM teams WHERE UPPER(teams.name) = picks.team")
    # Now change to not nullable, the desired setting (not that home_team is skipped).
    op.alter_column(table_name="lines", column_name="favored_team_id", nullable=False)
    op.alter_column(table_name="lines", column_name="underdog_team_id", nullable=False)
    op.alter_column(table_name="picks", column_name="team_id", nullable=False)
    # Drop the old column.
    op.drop_column(table_name="lines", column_name="favored_team")
    op.drop_column(table_name="lines", column_name="underdog_team")
    op.drop_column(table_name="lines", column_name="home_team")
    op.drop_column(table_name="picks", column_name="team")


def downgrade():
    pass
