South Bay Supercontest
======================

.. include:: badges.rst

An application for wagering against NFL lines.

Get started with any of the links in the menu.

.. toctree::
   :hidden:

   rules
   stack
   clients
   api
   legal
   indices
