Clients
=======

Python Client
-------------

There is a `python client <https://pypi.org/project/supercontest-client/>`_ to fetch all the data programmatically.

Web Client
----------

There is an interactive `web client <https://southbaysupercontest.com/graphql>`_ to query the data graph.
