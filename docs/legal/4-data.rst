Data Processing Agreement
=========================

*Last Updated: 2020-01-01*

This Data Processing Agreement ("Agreement") is entered into by and between South Bay Supercontest
("Data Controller") and the user ("Data Subject"), collectively referred to as the "Parties".

1. **Definitions**. "Personal Data" means any information relating to an identified or
identifiable natural person.

2. **Data Processing**. The Data Controller shall process the Personal Data only for the purposes
of providing services to the Data Subject in accordance with the terms of use of the website.

3. **Confidentiality**. The Data Controller shall ensure that any person it authorizes to
process the Personal Data shall respect the confidentiality of such data.

4. **Security**. The Data Controller shall implement appropriate technical and organizational
measures to ensure a level of security appropriate to the risk.

5. **Subprocessing**. The Data Controller shall not subcontract any of its processing operations
performed on behalf of the Data Subject under this Agreement without the prior written consent
of the Data Subject.

6. **Data Subject Rights**. The Data Controller shall assist the Data Subject in ensuring
compliance with the obligations pursuant to the rights of the data subject.

7. **Termination**. Upon termination of this Agreement, for whatever reason, the Data Controller
shall return all the Personal Data to the Data Subject and delete existing copies unless
Union or Member State law requires storage of the Personal Data.

8. **Governing Law**. This Agreement shall be governed by the laws of the United States.
