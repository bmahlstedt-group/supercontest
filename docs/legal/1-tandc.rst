Terms and Conditions
====================

*Last Updated: 2020-01-01*

1. **Introduction**. Welcome to South Bay Supercontest ("we", "us", "our"). By using our website
(our "Website"), you are agreeing to these Terms and Conditions. Please read them carefully.

2. **Using Our Website**. You may use our Website only as permitted by these Terms and Conditions
and any applicable laws. Don't misuse our Website. You may use our Website only as
permitted by law.

3. **Your Account**. You may need an account to use some of our services. You are responsible
for keeping your account username and password confidential. You are also responsible for any
account that you have access to and ensuring that all activities that occur in connection with
your account comply with these Terms and Conditions.

4. **Your Content**. Our Website allows you to submit content. You retain ownership of any
intellectual property rights that you hold in that content. When you upload or otherwise submit
content to our Website, you give us permission to use that content for the purpose of providing
services to you.

5. **Our Warranties and Disclaimers**. Other than as expressly set out in these terms or additional
terms, we do not make any specific promises about the website. For example, we do not make any
commitments about the content within the website, the specific function of the website, or their
reliability, availability, or ability to meet your needs. We provide the website “as is”.

6. **Liability for our Website**. To the extent permitted by law, we will not be responsible for
lost profits, revenues, or data, financial losses or indirect, special, consequential,
exemplary, or punitive damages.

7. **About these Terms**. We may modify these terms or any additional terms that apply to a
service to, for example, reflect changes to the law or changes to our Website. You should look
at the terms regularly. We will post notice of modifications to these terms on this page.
