End User License Agreement
==========================

*Last Updated: 2020-01-01*

This End User License Agreement ("Agreement") is between you and South Bay Supercontest
("we", "us", "our") and governs use of this website and all of its pages ("Website").
By using the Website, you agree to be bound by this Agreement.

1. **License**. We grant you a revocable, non-exclusive, non-transferable, limited license
to use the Website strictly in accordance with the terms of this Agreement.

2. **Restrictions**. You agree not to, and you will not permit others to license, sell, rent,
lease, assign, distribute, transmit, host, outsource, disclose or otherwise commercially exploit
the Website or make the Website available to any third party.

3. **Modification**. We reserve the right to modify, suspend or discontinue, temporarily or
permanently, the Website or any service to which it connects, with or without notice and
without liability to you.

4. **Indemnification**. You agree to indemnify and hold us harmless from any demands, loss,
liability, claims or expenses (including attorneys’ fees), made against us by any third party
due to, or arising out of, or in connection with your use of the Website.

5. **Severability**. If any provision of this Agreement is held to be unenforceable or invalid,
such provision will be changed and interpreted to accomplish the objectives of such provision to
the greatest extent possible under applicable law and the remaining provisions will continue
in full force and effect.

6. **Amendments to this Agreement**. We reserve the right, at our sole discretion, to modify or
replace this Agreement at any time. What constitutes a material change will be determined
at our sole discretion.
