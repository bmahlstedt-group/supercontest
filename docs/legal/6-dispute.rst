Dispute Resolution Policy
=========================

*Last Updated: 2020-01-01*

This Dispute Resolution Policy ("Policy") applies to all disputes raised by users ("you")
regarding the South Bay Supercontest website ("Website").

1. **Contact Us First**. If you have a dispute, we encourage you to contact us first.
We value our users and will attempt to resolve your issues directly. Please email us at
southbaysupercontest@gmail.com with the details of your dispute.

2. **Formal Dispute Notice**. If we are unable to resolve your dispute informally, you must send
a formal dispute notice to southbaysupercontest@gmail.com. The notice must include:

* Your full name and contact information.
* A detailed description of your dispute.
* The specific relief you are seeking.

3. **Response to Dispute Notice**. We will review your dispute notice and attempt to resolve
the dispute.

4. **Changes to This Policy**. We may update this Policy from time to time. We will notify you of
any changes by posting the new Policy on this page.
