.. image:: https://readthedocs.org/projects/supercontest/badge/?version=master
    :target: https://docs.southbaysupercontest.com/en/master/?badge=master
    :alt: Documentation Status

.. image:: https://gitlab.com/bmahlstedt/supercontest/badges/master/pipeline.svg
    :target: https://gitlab.com/bmahlstedt/supercontest/-/commits/master
    :alt: Build Status

.. image:: https://gitlab.com/bmahlstedt/supercontest/badges/master/coverage.svg
    :target: https://gitlab.com/bmahlstedt/supercontest/-/commits/master
    :alt: Coverage Report

.. image:: https://img.shields.io/pypi/v/supercontest-client.svg
    :target: https://pypi.org/project/supercontest-client/
    :alt: Client Package
