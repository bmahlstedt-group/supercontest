# Init

### Host system prerequisites

Go to the docker site and install docker. Modern versions come with compose.

To run docker without sudo: https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user

Create the docker group, add your user to it, and chown root:docker /var/run/docker.sock

Install the following tools, if your system doesn't have them: (follow the instructions on their websites, I won't copy them stale here)
* `make`
* `pyenv`
* `poetry`
* `psql` (postgres client, includes `pg_dump` and `pg_restore`)
* `redis-cli`

Check the docker-compose files for ports that the application will use, and make sure they're not already occupied.

Git clone this repository.

### Manually create private files

Create a file called `supercontest/config/app/private.py` with the following content:
```python
MAIL_PASSWORD = <>  # no longer the same as pg password, it's in sbsc's google account app passwords (generate new if no longer possessed)
APP_PASSWORD = ''  # southbaysupercontest@gmail.com's password for the site (in my browser)
SECRET_KEY = # run python -c "import random, string; print repr(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(32)));"

STRIPE_SECRET_KEY_API = ''           # find in stripe account
STRIPE_SECRET_KEY_API_TEST = ''      # find in stripe account

STRIPE_SECRET_KEY_WEBHOOK = ''       # find in stripe account
STRIPE_SECRET_KEY_WEBHOOK_TEST = ''  # copy this after running in the command line: stripe listen --forward-to localhost:8000/checkout_confirmation
```

Create a file called `supercontest/config/db/private.conf` with the following content:
```yml
POSTGRES_PASSWORD=<>  # find in my saved passwords, same as MAIL_PASSWORD but without quotes
```

Create a file called `supercontest/config/cache/private.conf` with the following content:
```yml
REDISCLI_AUTH=<>  # same as pg password but doubled (concatenated)
```

Create a file called `supercontest/config/db/private.pgpass` with the following content:
```yml
<host>:<port>:<dbname>:<username>:<password>
```
Then `chmod 0600 supercontest/config/db/private.pgpass`

### Start the desired service

Those commands will bring the services up. If no images exist, they will be built.
```bash
make build-start-prod  # on the cloud instance
make build-start-dev   # on the laptop
```

The prod app can be found at southbaysupercontest.com. The dev app can be found at `localhost:8000`. If in WSL, whitelist this port in Windows Firewall, inbound and outbound.

# Debugging

The dev app automatically starts with a debug toolbar that can be used in the frontend to analyze a substantial amount of important app/request metadata.

Check the `makefile` for other targets. Many assist with debugging. Some are discussed below.

Flask's CLI (`make flask-bash`) is used as a makefile of sorts for the alembic portions of the app.

You may also directly enter a python context of the app (in the container) for the following Python functionality of this application:
* running sqlalchemy queries through the orm
* committing lines
* etc

Use `make python` to simply enter a python interpreter with all app packages available. Use `make flask-python` when you want the flask elements (`current_app`, etc).

# Dependencies

Common poetry commands:
```bash
poetry install [--sync]        # install the deps from the lockfile into the local env (sync to remove any extras already there)
poetry lock [--no-update]      # sync your lockfile with your project toml (and update versions or not)
poetry update                  # runs the lock (with updated versions) then install
poetry update --dry-run        # no lock or install, just show what would be updated
poetry shell                   # to activate a subshell (`exit` to exit)
poetry env info --path         # to determine the path of the venv (then you can obv bin/activate and bin/deactivate instead of the subshell)
poetry add <dep> [--group <>]  # install a pkg and add it to the project's deps
poetry show --tree             # to show a graph of dependencies (transitive)
poetry <> --with <>            # to include other depgroups (like dev or test) in the above commands
```

# Changing Models

Modify the models directly: `supercontest/models/models.py`

Then `make flask-bash` to enter a shell with the flask CLI, and run `flask db <>`, which calls `flask-migrate`, which wraps alembic:
```bash
flask db history
flask db current
flask db revision -m <title of revision>    # creates a blank migration
flask db migrate -m <title of revision>     # infers the migration from your changes to the models
flask db upgrade [revision]
```

All **model** migrations go in `migrations/versions`, managed by alembic. Run these with the above commands.

The processes above will keep the database schema in sync with the ORM schema. However, there are other considerations. If you change the models (primarly just for adding/removing tables), you should also keep these in sync: (just follow the examples there)
* GraphQL: `supercontest/graphql/schema.py`
* Admin Panel, "Models" View: `supercontest/__init__.py`
* Marshmallow Serialization Schemas: `supercontest/models/schemas.py`

# Changing Data

All **data** changes go in `migrations/data_changes`, managed manually. Run these with `make flask-bash` and then `python migrations/data_changes/<>.py`. There are many examples in there; committing scores, adding users to leagues, etc.

There's an admin interface to add late picks for the current week.

**Important**: Do not update/insert with psql directly. There are computed cols that only live in the ORM layer, so they won't trigger when direct SQL changes are made. Be mindful of this when doing migrations.

**Important**: `Contestant.opponent_score` is intentionally denormalized. See [gitlab comment](https://gitlab.com/bmahlstedt/supercontest/-/issues/174#note_1319039914). Do not change one `score` without its corresponding object's `opponent_score`.

# Deployment

First run the tests and static analysis as necessary (check make targets).

Update the version in `pyproject.toml`.

Push your code, MR, and merge to master.

SSH into prod and run:
```
cd /code/supercontest
make stop-prod
git pull
make build-start-prod
```

Deployments do not automatically run database migrations. If your change involved the models, you need to upgrade the database. It's usually best to wait until off-hours, then run the standard deployment, then upgrade manually. SSH into prod and run:

```bash
make flask-bash
cd /sc/
flask db upgrade
```

If you need to upload a new version of the client to PyPI, check `client/ADMIN.md`.

# Networking

Check the AWS account for all resources (particularly Route53, ACM, Cloudfront, EC2, S3).

There are a few useful subdomains:
* `ssh.southbaysupercontest.com` and `ec2.southbaysupercontest.com` for instance access.
* `docs.southbaysupercontest.com` for readthedocs.
* `www.southbaysupercontest.com` is no longer supported.

ACM is configured for SAN as `*.southbaysupercontest.com`.

Assets are at `southbaysupercontest.com/assets`, distributed by cloudfront and stored on S3.

All requests (for app and static content) go through cloudfront. All origins (S3 and ELB) only allow ingress via cloudfront. Domain-specific filtering (localhost cannot request banners, southbaysupercontest.com can) is handled by WAF.

For static: Banners, brands, team logos, etc are routed at `/assets`, served by S3 (via CF). CSS and JS are at `/static`, served by the application. I'll clean this up later.

# Docs

Docs are built with sphinx and hosted on readthedocs (at an sbsc subdomain): [link](https://docs.southbaysupercontest.com)

It's integrated with gitlab. A branch version will automatically build and gate merge requests.

To build locally, run `make docs` then open `_readthedocs/html/index.html` with your browser. In vscode, you can right click the file and then select "Open With Live Server" from the context menu.

Remember that `docs/modules` can retain old autogenerated rst files which can have broken imports if you've moved modules around.

All docs for the application (including READMEs, etc) are located in `docs/` except for two:
* The toplevel `README.md`, which links to the hosted docs.
* The toplevel `ADMIN.md`, which is only for admin.

# Stripe

To test the end-to-end stripe payment system in dev mode, you have to:
* Install `stripe-cli` (see https://stripe.com/docs/stripe-cli)
* Run `stripe login` and follow the prompts to auth in the browser
* Run `stripe listen --forward-to localhost:8000/checkout_confirmation` then wait a few moments
* Copy the printed key to `STRIPE_SECRET_KEY_WEBHOOK_TEST` in `supercontest/config/app/private.py`

# Admin Interface

Go to [the admin panel](https://southbaysupercontest.com/admin/late_picks) and check out the
available functions there.

Only users logged in with an `Admin` role can access this.

This interface allows you to do tasks like:
* Add picks for a user after picks have locked for the week.
* Change the status of a game, if the APIs are being slow to update.
* Inspect all models of the application.
* Manually trigger a line fetch/commit.

# Banner

Every wednesday, deploy the banner. Upload to s3 (`/assets/banners/YYYY-WW-name.jpg` and copy to `/assets/banners/banner`), then run an invalidation on the latter path in cloudfront.

# Season Rollover

Overview: Get the site up, fixing any bugs from code-rot in the offseason. Then create migrations for new season/weeks as well as league. Then get prod site up, commit lines, submit picks, and fetch scores.

Prep (maybe needed, maybe not):
* Run `sudo firewall-cmd --zone=docker --change-interface=docker0` because the VPN will likely have changed settings.
* Removed "extras" from the compiled requirements file, pip was complaining about it.
* `sudo pip install virtualenv`
* `make virtualenv`
* `sed -i 's/\[.*\]//g' compiled-requirements.txt`
* Completely wiped and reinstalled docker. https://docs.docker.com/engine/install/fedora/.
* Enabled less-secure-app access on the sbsc google account.
* Generated a new ssh key and added to `authorized_keys` on the droplet (no, on EC2 now).
* Kill chrome (and other apps) while you're working. Takes a lot of memory. Your laptop is tiny.
* Had to go back to an older version of selenium base image, since ARM platform is not supported in latest.
* If the site reports the cert as expired, follow the instructions in [infra](https://gitlab.com/bmahlstedt/infra/-/blob/master/README.md).

Runbook for september 1, functional bringup:
* `make build-start-dev` locally, on your dev laptop.
* `make sync-db-dev` Which does: backup remote to local, change name to supercontest.dump, restore local from local.
* `make flask-bash` and `flask db revision -m "adding <> season"` like above.
* Then exit and `cd migrations/versions` and copy the content from a previous season creation. Use 2023-2024, since that has all the most recent stuff (like daylight savings shifts, etc). Modify top section as instructed.
* `make flask-bash` and `flask db upgrade`.
* Iterate until the app comes up locally. Click through the various pages and ensure they're not 500ing.
* Commit your changes and merge.
* Update the banner (instructions above in this README).
* On the prod VM: `cd code/supercontest && make down-prod`, `docker system prune -af`, `sudo apt upgrade`, `sudo apt autoremove`, `sudo reboot`, reconnect and `do-release-upgrade` (this will reboot the system).
* Log back in and run the deployment of your changes, including migrating the prod db, following the instructions above in the readme.
* Add yourself to the paid league. `make flask-python` then `from supercontest.dbsession.commits import add_user_to_current_paid_league` and then `add_user_to_current_paid_league("brian.mahlstedt@gmail.com")`.

Runbook for wednesday night of week 1, scrapers for lines/scores:
* Follow the usual instructions to commit the lines (they obviously must be posted already). If the westgate site structure has changed, the webscraper will obviously fail. Update as necessary.
* Live NFL scores are hard to obtain for free, and APIs often change. You usually need to implement a new webscraper every season. Google for alternatives, and comment the old out (but keep around just in case).
* Sometimes the lines are out but the livescores are not. Use `core.scores.fetch_scores` to point to `_fetch_scores_fake()` and manually fill in the data in that function. Then run the commit-lines functionality as usual from the admin panel (in dev first, then prod).
* Enable all the schedules in EventBridge (Commit*, Email*, etc).

Runbook for the last day of the last week:
* It will automatically change to postseason at wed 5pm, and the home page will become the leaderboard.
* Just create an image with black background and the winner's name and put that as the banner.
* Go into EventBridge and disable the 4 schedules.
